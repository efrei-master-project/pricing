﻿FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build
RUN dotnet tool install --global dotnet-ef
ENV PATH="${PATH}:/root/.dotnet/tools"
ENV DOTNET_WATCH_RESTART_ON_RUDE_EDIT=1
WORKDIR /app
EXPOSE 5001
EXPOSE 5000

RUN apt-get update && \
    apt-get install -y curl && \
    apt-get install -y libpng-dev libjpeg-dev curl libxi6 build-essential ca-certificates libgl1-mesa-glx && \
    curl -sL https://deb.nodesource.com/setup_lts.x | bash - && \
    apt-get install -yq nodejs && \
    npm install npm 

CMD ["dotnet", "watch", "run"]