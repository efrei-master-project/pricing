﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using pricing.Context;

#nullable disable

namespace pricing.Migrations
{
    [DbContext(typeof(PricingContext))]
    [Migration("20220513151835_addModelsithNewPackage")]
    partial class addModelsithNewPackage
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "6.0.5")
                .HasAnnotation("Relational:MaxIdentifierLength", 64);

            modelBuilder.Entity("pricing.Models.Currency", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    b.Property<string>("CurrencyCode")
                        .HasColumnType("longtext");

                    b.Property<string>("CurrencyName")
                        .HasColumnType("longtext");

                    b.HasKey("Id");

                    b.ToTable("Currencies");
                });

            modelBuilder.Entity("pricing.Models.PriceData", b =>
                {
                    b.Property<string>("Symbol")
                        .HasColumnType("varchar(255)")
                        .HasColumnOrder(1);

                    b.Property<DateTime>("Date")
                        .HasColumnType("datetime(6)")
                        .HasColumnOrder(0);

                    b.Property<float>("Close")
                        .HasColumnType("float");

                    b.Property<int?>("CurrencyId")
                        .HasColumnType("int");

                    b.Property<float>("High")
                        .HasColumnType("float");

                    b.Property<float>("Low")
                        .HasColumnType("float");

                    b.Property<float>("Open")
                        .HasColumnType("float");

                    b.HasKey("Symbol", "Date");

                    b.HasIndex("CurrencyId");

                    b.ToTable("PriceData");
                });

            modelBuilder.Entity("pricing.Models.PriceData", b =>
                {
                    b.HasOne("pricing.Models.Currency", "Currency")
                        .WithMany()
                        .HasForeignKey("CurrencyId");

                    b.Navigation("Currency");
                });
#pragma warning restore 612, 618
        }
    }
}
