﻿using System;
using System.Text.Json;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.SpaServices.ReactDevelopmentServer;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using pricing.Context;
using pricing.Exceptions;
using pricing.Models;
using pricing.Services;
using pricing.Repository;

namespace pricing
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy(name: "AllowOrigin",
                    builder =>
                    {
                        builder.WithOrigins("http://localhost:5000", "https://localhost:5001")
                            .AllowAnyHeader()
                            .AllowAnyMethod();
                    });
            });

            services.AddControllersWithViews();
            // In production, the React files will be served from this directory
            services.AddSpaStaticFiles(configuration => { configuration.RootPath = "ClientApp/build"; });
            string connectionString = Configuration.GetConnectionString("DefaultConnection");
            services.AddDbContextPool<PricingContext>(builder =>
                builder.UseMySql(
                    Configuration.GetConnectionString("DefaultConnection"),
                    ServerVersion.AutoDetect(connectionString)
                ));
            services.AddScoped<MarketStackApi>();
            services.AddScoped<PricerService>();
            services.AddDistributedMemoryCache();
            services.AddSession();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseCors("AllowOrigin");

            app.UseExceptionHandler(errorApp =>
            {
                errorApp.Run(async context =>
                {
                    var exception =
                        context.Features.Get<IExceptionHandlerPathFeature>()?.Error;
                    context.Response.ContentType = "application/json";
                    var description = "";
                    context.Response.StatusCode = 500;
                    
                    if (exception is ApiException e)
                    {
                        context.Response.StatusCode = e.StatusCode;
                        description = e.Description;
                    }


                    var stream = context.Response.Body;
                    await JsonSerializer.SerializeAsync(stream, new Error()
                    {
                        Message = exception?.Message,
                        Name = exception?.GetType().Name,
                        Description = description
                    });
                });
            });

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseSpaStaticFiles();

            app.UsePathBase(new PathString("/api"));
            app.UseRouting();
            app.UseSession();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller}/{action=Index}/{id?}");
            });

            app.UseSpa(spa =>
            {
                spa.Options.SourcePath = "ClientApp";

                if (env.IsDevelopment() || env.EnvironmentName == "local")
                {
                    spa.UseReactDevelopmentServer(npmScript: "start");
                }
            });
        }
    }
}