using System.IO;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using pricing.Models;

namespace pricing.Context
{
    public class PricingContext : DbContext
    {
        
        public PricingContext(DbContextOptions<PricingContext> options): base(options)
        {
        }
        
        public DbSet<Currency> Currencies { get; set; }
        public DbSet<PriceData> PriceData { get; set; }
        public DbSet<Equity> Equities { get; set; }
        public DbSet<Transaction> Transactions { get; set; }
        

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder) 
        {
            if (!optionsBuilder.IsConfigured)
            {
                IConfigurationRoot configuration = new ConfigurationBuilder()
                    .SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("appsettings.json")
                    .Build();
                var connectionString = configuration.GetConnectionString("ConnectionStrings:DefaultConnection");
                optionsBuilder.UseMySql(connectionString, ServerVersion.AutoDetect(connectionString));
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<PriceData>().HasKey(data => new
            {
                data.Symbol,
                data.Date
            });
        }
    }
}