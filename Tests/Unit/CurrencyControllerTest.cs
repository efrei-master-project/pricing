using System.Collections.ObjectModel;
using System.Linq;
using Moq;
using Microsoft.Extensions.Logging;
using pricing.Controllers;
using pricing.Models;
using pricing.Repository;
using Xunit;
using Xunit.Abstractions;

namespace pricing.Tests.Unit;

public class CurrenciesControllerTest
{
    private readonly ITestOutputHelper output;

    public CurrenciesControllerTest(ITestOutputHelper output)
    {
        this.output = output;
    }

    private static ILogger<CurrenciesController> GetLoggerMock()
    {
        return new Mock<ILogger<CurrenciesController>>().Object;
    }

    [Fact]
    public void FetchCurrencies()
    {
        Mock<IRepository> mockObject = new Mock<IRepository>();
        mockObject.Setup(repository => repository.FindAll()).Returns(GetTestCurrencies());

        var controller = new CurrenciesController(GetLoggerMock(), mockObject.Object);

        var result = controller.FetchCurrencies();

        Assert.Equal(2, result.Count());
        
        foreach (var currency in result.ToArray())
        {
            Assert.IsType<Currency>(currency);
        }

        var firstCurrency = (Currency) result.First();
        Assert.Equal(1, firstCurrency.Id);
        Assert.Equal("EUR", firstCurrency.CurrencyCode);
        Assert.Equal("euro", firstCurrency.CurrencyName);
        
        var secondCurrency = (Currency) result.Skip(1).First();
        Assert.Equal(2, secondCurrency.Id);
        Assert.Equal("USD", secondCurrency.CurrencyCode);
        Assert.Equal("dollar", secondCurrency.CurrencyName);
    }

    private static Collection<Model> GetTestCurrencies()
    {
        return new Collection<Model>()
        {
            new Currency()
            {
                Id = 1,
                CurrencyCode = "EUR",
                CurrencyName = "euro"
            },
            new Currency()
            {
                Id = 2,
                CurrencyCode = "USD",
                CurrencyName = "dollar"
            }
        };
    }
}