namespace pricing.Models;

public class Error
{
    public string Message { get; set; }
    public string Name { get; set; }
    public string Description { get; set; }
}