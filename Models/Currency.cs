
using System.ComponentModel.DataAnnotations;

namespace pricing.Models
{
    public class Currency : Model
    {
        [Key]
        public int Id { get; set; }
        
        public string CurrencyName { get; set; }
        public string CurrencyCode { get; set; }
    }
}