using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace pricing.Models
{
    public class PriceData : Model
    {
        [Key, Column(Order=0)]
        public DateTime? Date { get; set; }
        
        [Key, Column(Order=1)]
        public string Symbol { get; set; }
        public Currency Currency { get; set; }
        public float? Open { get; set; }
        public float? Close { get; set; }
        public float? High { get; set; }
        public float? Low { get; set; }
    }
}