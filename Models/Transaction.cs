using System.ComponentModel.DataAnnotations;

namespace pricing.Models;

public class Transaction : Model
{
    [Key] 
    public int Id { get; set; }
    public float Amount { get; set; }
    public int Quantity { get; set; }
    public string BuyerName { get; set; }
    public Currency Currency { get; set; }
    public string Symbol { get; set; }
    public int MaturityYear { get; set; }
    public float Strike { get; set; }
}