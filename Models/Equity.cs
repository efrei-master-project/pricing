using System.ComponentModel.DataAnnotations;

namespace pricing.Models;

public class Equity : Model
{
    [Key]
    public string Symbol { get; set; }
    public string Name { get; set; }
}