#COLORS
GREEN  := $(shell tput -Txterm setaf 2)
WHITE  := $(shell tput -Txterm setaf 7)
YELLOW := $(shell tput -Txterm setaf 3)
RESET  := $(shell tput -Txterm sgr0)

# And add help text after each target name starting with '\#\#'
# A category can be added with @category
HELP_COMMAND = \
    %help; \
    while(<>) { push @{$$help{$$2 // 'Commands'}}, [$$1, $$3] if /^([a-zA-Z\-]+)\s*:.*\#\#(?:@([a-zA-Z\-]+))?\s(.*)$$/ }; \
    print "usage: make [target]\n\n"; \
    for (sort keys %help) { \
    print "${WHITE}$$_:${RESET}\n"; \
    for (@{$$help{$$_}}) { \
    $$sep = " " x (32 - length $$_->[0]); \
    print "  ${YELLOW}$$_->[0]${RESET}$$sep${GREEN}$$_->[1]${RESET}\n"; \
    }; \
    print "\n"; }

help: ##@Help Show the list of the commands
	@perl -e '$(HELP_COMMAND)' $(MAKEFILE_LIST)


db-install: ## Install database
	dotnet ef database drop -v --no-color
	dotnet ef database update -v --no-color

test: ## Run tests
	dotnet test --coverage

db-add-migration: ## Add Migration 
	rm -Rf obj bin
	dotnet ef migrations add $(filter-out $@,$(MAKECMDGOALS)) -v --no-color
