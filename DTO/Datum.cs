﻿using System;

namespace pricing.DTO
{
    public class Datum
    {
        public double Open { get; set; }
        public double High { get; set; }
        public double Low { get; set; }
        public double Close { get; set; }
        public double Volume { get; set; }
        public double Last { get; set; }
        public double Adj_high { get; set; }
        public double Adj_low { get; set; }
        public double Adj_close { get; set; }
        public double Adj_open { get; set; }
        public double Adj_volume { get; set; }
        public double Split_factor { get; set; }
        public double Dividend { get; set; }
        public string Symbol { get; set; }
        public string Exchange { get; set; }
        public DateTime Date { get; set; }
    }
}
