﻿using System.Collections.Generic;

namespace pricing.DTO
{
    public class Root
    {
        public Pagination Pagination { get; set; }
        public List<Datum> Data { get; set; }
    }

}
