﻿FROM mcr.microsoft.com/dotnet/aspnet:6.0 AS base
WORKDIR /app
EXPOSE 5001
EXPOSE 5000
ENV ASPNETCORE_ENVIRONMENT="Production"

FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build

RUN apt-get update && \
    apt-get install -y curl && \
    apt-get install -y libpng-dev libjpeg-dev curl libxi6 build-essential libgl1-mesa-glx && \
    curl -sL https://deb.nodesource.com/setup_lts.x | bash - && \
    apt-get install -y nodejs

WORKDIR /src
COPY ["pricing.csproj", "./"]
RUN dotnet restore "pricing.csproj"
COPY . .
WORKDIR "/src/"
RUN dotnet build "pricing.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "pricing.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .

ENTRYPOINT ["dotnet", "pricing.dll"]
