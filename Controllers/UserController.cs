using System;
using System.Collections.Generic;
using System.Text.Json;
using System.Threading.Tasks;
using Firebase.Auth;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using pricing.DTO;
using pricing.Exceptions;
using User = pricing.Models.User;

namespace pricing.Controllers;

[ApiController]
[Route("[controller]")]
public class UserController : DefaultController
{
    private FirebaseAuthProvider auth;

    public UserController(IConfiguration config)
    {
        string apiKey = config.GetValue<string>("firebaseApiKey");
        auth = new FirebaseAuthProvider(
            new FirebaseConfig(apiKey));
    }

    [HttpGet]
    public async Task<Firebase.Auth.User> Index()
    {
        return await IsLoggedIn(auth);
    }

    [HttpPost]
    [Route("sign-in")]
    public async Task<IActionResult> SignIn(User userModel)
    {
        try
        {
            var fbAuthLink = await auth
                .SignInWithEmailAndPasswordAsync(userModel.Email, userModel.Password);

            string token = fbAuthLink.FirebaseToken;

            //saving the token in a session variable
            if (token != null)
            {
                HttpContext.Session.SetString("_UserToken", token);

                return StatusCode(200, new Dictionary<string,string>{{"token",token}});
            }

            throw new ApiException(500,"Error in sign in");
        }
        catch (FirebaseAuthException exception)
        {
            var response = JsonConvert.DeserializeObject<FirebaseError>(exception.ResponseData);
            
            throw new ApiException(401,exception.Reason.ToString(), response?.error.message);
        }
    }

    [HttpPost]
    [Route("sign-out")]
    public new void SignOut()
    {
        HttpContext.Session.Remove("_UserToken");
    }
    
    

}