using System;
using System.Net.Http;
using System.Threading.Tasks;
using Firebase.Auth;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace pricing.Controllers;

public class DefaultController: ControllerBase
{
    public async Task<Firebase.Auth.User> IsLoggedIn(FirebaseAuthProvider auth)
    {
        string token = HttpContext.Session.GetString("_UserToken");
        
        if (token == null)
        {
            throw new ("test");
        }

        var user = await auth.GetUserAsync(token);
        
        return user;
    }
}