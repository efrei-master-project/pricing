using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using pricing.Models;
using pricing.Repository;

namespace pricing.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CurrenciesController : DefaultController
    {
        private readonly ILogger<CurrenciesController> _logger;
        private readonly IRepository _currencyRepository;

        public CurrenciesController(ILogger<CurrenciesController> logger, IRepository repository)
        {
            _logger = logger;
            _currencyRepository = repository;
        }

        [HttpGet]
        public ICollection<Model> FetchCurrencies()
        {
            return _currencyRepository.FindAll();
        }

        [HttpGet]
        [Route("{id:int}")]
        public Model FetchCurrency(int id)
        {
            return _currencyRepository.FindOneByIdentifiers(new Currency{Id = id});
        }
    }
}