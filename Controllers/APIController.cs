﻿using Microsoft.AspNetCore.Mvc;
using pricing.DTO;
using pricing.Services;

namespace pricing.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ApiController : DefaultController
    {

        private readonly MarketStackApi _marketStackApi;

        public ApiController(MarketStackApi marketStackApi)
        {
            _marketStackApi = marketStackApi;
        }

        [HttpGet]
        public Root CatchApiData()
        {
            return _marketStackApi.GetValuesByDate("AAPL","2022-06-16");
        }
    }
}
