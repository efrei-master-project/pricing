﻿using System;
using System.Collections.Generic;
using System.IO;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using pricing.DTO;
using pricing.Services;

namespace pricing.Controllers
{
    [ApiController]
    [Route("data")]
    public class PriceDataController : DefaultController
    {
        private readonly MarketStackApi _marketStackApi;
        private readonly PricerService _pricer;

        public PriceDataController(MarketStackApi marketStackApi, PricerService pricer)
        {
            _marketStackApi = marketStackApi;
            _pricer = pricer;
        }

        [HttpGet("tends")]
        public Dictionary<string, List<Datum>> GetTendsData([FromQuery] List<string> symbols)
        {
            /*var tendsData = new Dictionary<string,List<Datum>>();
            
            Root data = _api.GetValuesBySymbols(symbols);
            
            foreach (Datum price in data.Data)
            {
                if (!tendsData.ContainsKey(price.Symbol))
                {
                    tendsData.Add(price.Symbol, new List<Datum>());
                }
                
                tendsData[price.Symbol] = tendsData[price.Symbol].Prepend(price).ToList();
            }
            
            return tendsData;*/


            var reader = new StreamReader("Fixtures/tends.json");
            var json = reader.ReadToEnd();

            return JsonConvert.DeserializeObject<Dictionary<string, List<Datum>>>(json);
        }

        [HttpGet("tend")]
        public List<Datum> GetTendData(string symbol)
        {
            Root data = _marketStackApi.GetValuesBySymbol(symbol);

            return data.Data;
        }

        [HttpGet("pricing-data")]
        public List<Datum> GetDataPricingGraph(int gap, string unit, string symbol)
        {
            DateTime date;
            switch (unit)
            {
                case "month":
                    date = DateTime.Today.AddMonths(-gap);
                    break;
                case "day":
                    date = DateTime.Today.AddDays(-gap);
                    break;
                case "week":
                    date = DateTime.Today.AddDays(-(gap % 7));
                    break;
                case "year":
                    date = DateTime.Today.AddYears(-gap);
                    break;
                default:
                    date = DateTime.Today.AddMonths(-gap);
                    break;
            }

            Console.WriteLine("remove " + gap + " " + unit);
            Console.WriteLine(date.Date);

            Root data = _marketStackApi.GetValuesByDate(symbol, date.ToString("yyyy-MM-dd"));
            return data.Data;
        }


        [HttpGet]
        [Route("pricer")]
        public Dictionary<string, double> CalculatePrice(double strikePrice, int maturityTime,
            string stockName)
        {
            return _pricer.Calculate(stockName, strikePrice, strikePrice);
        }
    }
}