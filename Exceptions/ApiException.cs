using System;

namespace pricing.Exceptions;

public class ApiException : Exception
{
    public int StatusCode { get; set; }
    public string Description { get; set; }

    public ApiException(int statusCode, string message) : base(message)
    {
        this.StatusCode = statusCode;
        Description = "";
    }
    public ApiException(int statusCode, string message, string description) : base(message)
    {
        this.StatusCode = statusCode;
        Description = description;
    }
}