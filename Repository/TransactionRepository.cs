using System;
using System.Collections.Generic;
using System.Linq;
using pricing.Context;
using pricing.Models;


namespace pricing.Repository
{
    public class TransactionRepository : IRepository
    {
        private readonly PricingContext _context;

        public TransactionRepository(PricingContext context)
        {
            _context = context;
        }

        public ICollection<Model> FindAll()
        {
            return _context.Transactions.ToArray();
        }

        public void Insert(Model model)
        {
            if (model is not Transaction transaction)
            {
                throw new Exception("This object must be Transaction type");
            }

            _context.Transactions.Add(transaction);
        }

        public Model FindOne(Model model)
        {
            throw new NotImplementedException();
        }

        public Model FindOneByIdentifiers(Model model)
        {
            throw new NotImplementedException();
        }

        public bool Remove(Model model)
        {
            throw new NotImplementedException();
        }

        public bool RemoveById(Model model)
        {
            throw new NotImplementedException();
        }

        public ICollection<Model> FindBy(Model model)
        {
            throw new NotImplementedException();
        }

        public Model FindOneById(int id)
        {
            return _context.Transactions.Find(id);
        }

        public void Save()
        {
            throw new NotImplementedException();
        }

    }

}