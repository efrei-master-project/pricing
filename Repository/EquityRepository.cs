using System;
using System.Collections.Generic;
using System.Linq;
using pricing.Context;
using pricing.Models;

namespace pricing.Repository
{
    public class EquityRepository : IRepository
    {
        private readonly PricingContext _context;

        public EquityRepository(PricingContext context)
        {
            _context = context;
        }

        public ICollection<Model> FindAll()
        {
            return _context.Equities.ToArray();
        }

        public void Insert(Model model)
        {
            if (model is not Equity equity)
            {
                throw new Exception("This object must be equity type");
            }

            _context.Equities.Add(equity);
        }

        public Model FindOne(Model model)
        {
            throw new NotImplementedException();
        }

        public Model FindOneByIdentifiers(Model model)
        {
            throw new NotImplementedException();
        }

        public bool Remove(Model model)
        {
            throw new NotImplementedException();
        }

        public bool RemoveById(Model model)
        {
            throw new NotImplementedException();
        }

        public ICollection<Model> FindBy(Model model)
        {
            throw new NotImplementedException();
        }

        public Model FindOneById(int id)
        {
            return _context.Equities.Find(id);
        }

        public void Save()
        {
            throw new NotImplementedException();
        }
    }
}