using System;
using System.Collections.Generic;
using System.Linq;
using pricing.Context;
using pricing.Models;

namespace pricing.Repository
{
    public class PriceDataRepository : IRepository
    {
        private readonly PricingContext _context;

        public PriceDataRepository(PricingContext context)
        {
            _context = context;
        }

        public ICollection<Model> FindAll()
        {
            return _context.PriceData.ToArray();
        }

        public void Insert(Model model)
        {
            if (model is not PriceData priceData)
            {
                throw new Exception("This object must be PriceData type");
            }

            _context.PriceData.Add(priceData);
        }

        public Model FindOne(Model model)
        {
            if (model is not PriceData priceData)
            {
                throw new Exception("This object must be PriceData type");
            }

            return _context.PriceData.Find(priceData);
        }

        public Model FindOneByIdentifiers(Model model)
        {
            if (model is not PriceData priceData)
            {
                throw new Exception("This object must be PriceData type");
            }

            return _context.PriceData.Find(priceData.Date, priceData.Symbol);
        }

        public bool Remove(Model model)
        {
            if (model is not PriceData priceData)
            {
                throw new Exception("This object must be PriceData type");
            }

            try
            {
                _context.PriceData.Remove(priceData);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            }

            return true;
        }

        public bool RemoveById(Model model)
        {
            if (model is not PriceData priceData)
            {
                throw new Exception("This object must be PriceData type");
            }

            var priceDataDelete = _context.PriceData.Find(priceData.Date, priceData.Symbol);
            if (priceDataDelete == null)
            {
                return false;
            }

            try
            {
                _context.PriceData.Remove(priceDataDelete);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            }

            _context.SaveChanges();
            return true;
        }

        public ICollection<Model> FindBy(Model model)
        {
            if (model is not PriceData priceData)
            {
                throw new Exception("This object must be Currency type");
            }

            bool Filter(PriceData p)
            {
                var res = true;
                if (priceData.Date != null) res = priceData.Date == p.Date;
                if (priceData.Symbol != null) res = res && priceData.Symbol == p.Symbol;
                if (priceData.Currency != null) res = res && priceData.Currency == p.Currency;
                if (priceData.Open != null) res = res && priceData.Open.Equals(p.Open);
                if (priceData.Close != null) res = res && priceData.Close.Equals(p.Close);
                if (priceData.High != null) res = res && priceData.High.Equals(p.High);
                if (priceData.Low != null) res = res && priceData.Low.Equals(p.Low);
                
                return res;
            }

            return _context.PriceData.Where((Func<PriceData, bool>) Filter).ToArray();
        }

        public void Save()
        {
            _context.SaveChanges();
        }
    }
}