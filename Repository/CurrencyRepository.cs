using System;
using System.Collections.Generic;
using System.Linq;
using pricing.Context;
using pricing.Models;

namespace pricing.Repository
{
    public class CurrencyRepository : IRepository
    {
        private readonly PricingContext _context;

        public CurrencyRepository(PricingContext context)
        {
            _context = context;
        }

        public ICollection<Model> FindAll()
        {
            return _context.Currencies.ToArray();
        }
        
        public void Insert(Model model)
        {
            if (model is not Currency currency)
            {
                throw new Exception("This object must be Currency type");
            }

            _context.Currencies.Add(currency);
        }

        public Model FindOne(Model model)
        {
            throw new NotImplementedException();
        }

        public Model FindOneByIdentifiers(Model model)
        {
            return _context.Currencies.Find(model);
        }

        public bool Remove(Model model)
        {
            throw new NotImplementedException();
        }

        public bool RemoveById(Model model)
        {
            throw new NotImplementedException();
        }

        public ICollection<Model> FindBy(Model model)
        {
            throw new NotImplementedException();
        }

        public void Save()
        {
            throw new NotImplementedException();
        }
    }
}