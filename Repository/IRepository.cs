using System.Collections.Generic;
using pricing.Models;

namespace pricing.Repository;

public interface IRepository
{
    public ICollection<Model> FindAll();

    public void Insert(Model model);

    public Model FindOne(Model model);
        
    public Model FindOneByIdentifiers(Model model);

    public bool Remove(Model model);

    public bool RemoveById(Model model);

    public ICollection<Model> FindBy(Model model);
    public void Save();
}