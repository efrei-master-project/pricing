﻿using Newtonsoft.Json;
using pricing.DTO;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;

namespace pricing.Services
{
    public class MarketStackApi
    {
        private readonly HttpClient _client;
        private static string _url = "http://api.marketstack.com/v1";
        private readonly string _key;

        private static readonly JsonSerializerSettings JsonSettings = new JsonSerializerSettings()
        {
            NullValueHandling = NullValueHandling.Ignore,
            MissingMemberHandling = MissingMemberHandling.Ignore
        };

        public MarketStackApi(IConfiguration config)
        {
            _client = new HttpClient();
            _key = config.GetValue<string>("marketStackApiKey");
        }

        public Root GetValuesBySymbol(string stock)
        {
            Dictionary<string, string> parameters = new Dictionary<string, string>
            {
                {"symbols", stock}
            };
            
            string endpoint = "eod";

            var json = Request(_client, endpoint, parameters).GetAwaiter().GetResult();
            Root data = JsonConvert.DeserializeObject<Root>(json, JsonSettings);

            return data;
        }
        
        

        public Root GetValuesBySymbols(List<string> symbols)
        {
            Dictionary<string, string> parameters = new Dictionary<string, string>
            {
                {"symbols", string.Join(",",symbols.ToArray())}
            };
            
            string endpoint = "eod";

            var json = Request(_client, endpoint, parameters).GetAwaiter().GetResult();
            Root data = JsonConvert.DeserializeObject<Root>(json, JsonSettings);

            return data;
        }

        public Root GetValuesByDate(string stock, string dateFrom)
        {
            string endpoint = "eod";
            Dictionary<string, string> parameters = new Dictionary<string, string>
            {
                {"symbols", stock},
                {"date_from", dateFrom}
            };

            var json = Request(_client, endpoint, parameters).GetAwaiter().GetResult();
            Root data = JsonConvert.DeserializeObject<Root>(json, JsonSettings);

            return data;
        }

        public Root GetValuesForPricing(string stock)
        {
            string endpoint = "eod/latest";
            Dictionary<string, string> parameters = new Dictionary<string, string>
            {
                {"symbols", stock}
            };

            var json = Request(_client, endpoint, parameters).GetAwaiter().GetResult();
            
            Root data = JsonConvert.DeserializeObject<Root>(json, JsonSettings);

            return data;
        }

        private async Task<string> Request(HttpClient client, string endpoint, Dictionary<string, string> parameters)
        {
            string url = $"{_url}/{endpoint}?access_key={_key}";

            foreach (var (parameter, index) in parameters)
            {
                url += $"&{parameter}={index}";
            }
            
            var response = await client.GetAsync(url);

            if (response.IsSuccessStatusCode)
            {
                return await response.Content.ReadAsStringAsync();
            }

            throw new Exception(await response.Content.ReadAsStringAsync());
        }
    }
}