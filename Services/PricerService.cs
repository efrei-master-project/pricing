﻿using System;
using System.Collections.Generic;
using System.Linq;
using pricing.DTO;
using pricing.Pages;

namespace pricing.Services;

public class PricerService
{
    const double Rfr = 0.04; //Risk Free Rate
    private readonly MarketStackApi _marketStackApi;

    public PricerService(MarketStackApi marketStackApi)
    {
        _marketStackApi = marketStackApi;
    }

    public double StockPrice(string stockName)
    {
        Root stokPrice = _marketStackApi.GetValuesForPricing(stockName);

        return stokPrice.Data[0].Close;
    }

    public List<double> StockData(string stockName, string date)
    {
        List<double> priceList = new List<double>();
        Root stockData = _marketStackApi.GetValuesByDate(stockName, date);

        foreach (var datum in stockData.Data)
        {
            priceList.Add(datum.Close);
        }

        return priceList;
    }

    public double CallPrice(double strike, double maturity, double volatility, double stockPrice)
    {
        Console.WriteLine("strike : " + strike);
        Console.WriteLine("maturity : " + maturity);
        Console.WriteLine("volatility : " + volatility);
        Console.WriteLine("stockPrice : " + stockPrice);
        
        double sPDividedS = stockPrice / strike;
        double lnss = Math.Log(sPDividedS);
        double numerateur = (Math.Pow(volatility, 2) / 2 + Rfr) * maturity + lnss;
        double denominateur = Math.Sqrt(maturity) * volatility;

        double d1 = numerateur / denominateur;
        double d2 = d1 - volatility * Math.Sqrt(maturity);

        return stockPrice * Gauss(d1) - (strike / Math.Exp(Rfr * maturity)) * Gauss(d2);
    }

    public double PutPrice(string stockName, double strike, double maturity, double callPrice, double stockPrice)
    {
        return callPrice + strike * Math.Exp(-Rfr * maturity) - stockPrice;
    }

    public Dictionary<string, double> Calculate(string stockName, double strike, double maturity)
    {
        var date = DateTime.Now.AddYears(-1);
        var month = date.Month < 10 ? "0" + date.Month : "" + date.Month;
        var day = date.Day < 10 ? "0" + date.Day : "" + date.Day;
        double volatility = Volatility(stockName, $"{date.Year}-{month}-{day}");
        double stockPrice = StockPrice(stockName);
        var callPrice = CallPrice(strike, maturity, volatility, stockPrice);

        Console.Write(callPrice);
        return new Dictionary<string, double>()
        {
            {"call", callPrice},
            {"put", PutPrice(stockName, strike, maturity, callPrice, stockPrice)},
            {"volatility", volatility},
            {"riskFreeRate", Rfr},
            {"stock", stockPrice}
        };
    }

    public static double Gauss(double z)
    {
        // input = z-value (-inf to +inf)
        // output = p under Standard Normal curve from -inf to z
        // e.g., if z = 0.0, function returns 0.5000
        // ACM Algorithm #209
        double y; // 209 scratch variable
        double p; // result. called 'z' in 209
        double w; // 209 scratch variable
        if (z == 0.0)
            p = 0.0;
        else
        {
            y = Math.Abs(z) / 2;
            if (y >= 3.0)
            {
                p = 1.0;
            }
            else if (y < 1.0)
            {
                w = y * y;
                p = ((((((((0.000124818987 * w
                            - 0.001075204047) * w + 0.005198775019) * w
                          - 0.019198292004) * w + 0.059054035642) * w
                        - 0.151968751364) * w + 0.319152932694) * w
                      - 0.531923007300) * w + 0.797884560593) * y * 2.0;
            }
            else
            {
                y = y - 2.0;
                p = (((((((((((((-0.000045255659 * y
                                 + 0.000152529290) * y - 0.000019538132) * y
                               - 0.000676904986) * y + 0.001390604284) * y
                             - 0.000794620820) * y - 0.002034254874) * y
                           + 0.006549791214) * y - 0.010557625006) * y
                         + 0.011630447319) * y - 0.009279453341) * y
                       + 0.005353579108) * y - 0.002141268741) * y
                     + 0.000535310849) * y + 0.999936657524;
            }
        }

        if (z > 0.0)
            return (p + 1.0) / 2;
        else
            return (1.0 - p) / 2;
    }

    public double Volatility(string stockName, string date)
    {
        List<double> listEcart = new List<double>();
        List<double> variationMoyenneList = new List<double>();
        List<double> stockdate = StockData(stockName, date);

        for (int i = 0; i < stockdate.Count - 1; i++)
        {
            listEcart.Add(((stockdate[i + 1] - stockdate[i]) / stockdate[i]) * 100);
        }

        double firstEcart = listEcart.Average();
        for (int i = 0; i < stockdate.Count - 1; i++)
        {
            variationMoyenneList.Add(listEcart[i] - firstEcart);
            variationMoyenneList[i] = Math.Pow(variationMoyenneList[i], 2);
        }

        double meanCarreEcarts = variationMoyenneList.Average();
        double ecartType = Math.Sqrt(meanCarreEcarts);
        double volatility = Math.Sqrt(ecartType);

        return volatility;
    }
}