import React, {createContext} from 'react'
import firebaseConfig from './firebaseConfig';
import {initializeApp, getApps} from 'firebase/app'
import {getAuth, onAuthStateChanged} from "firebase/auth";
import store from './store';

// we create a React Context, for this to be accessible
// from a component later
const FirebaseContext = createContext(null)
export {FirebaseContext}

export default ({children}) => {
    let firebase = {
        app: null
    }
    
    if (!getApps().length) {
        const app = initializeApp(firebaseConfig);
        firebase = {
            app: app
        }
    }

    const auth = getAuth();
    onAuthStateChanged(auth, (user) => {
        if (user && user.emailVerified) {
            
        } else {
            
            
        }
    });


    return (
        <FirebaseContext.Provider value={firebase}>
            {children}
        </FirebaseContext.Provider>
    )
}