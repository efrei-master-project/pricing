import {configureStore} from "@reduxjs/toolkit";


const initialState = {
    user: null,
    currentPage: ""
}

const saveToLocalStorage = (state) => {
    try {
        localStorage.setItem('state', JSON.stringify(state));
    } catch (e) {
        console.error(e);
    }
};


const loadFromLocalStorage = () => {
    try {
        const stateStr = localStorage.getItem('state');
        
        return stateStr ? JSON.parse(stateStr) : initialState;
    } catch (e) {
        console.error(e);
        return initialState;
    }
};

export const reducer = (state , action) => {
    switch (action.type) {
        case 'sign-in':
            return {
                ...state,
                user: action.user
            };
        case 'user-update':
            return {
                ...state,
                user: action.user
            };
        case 'sign-out':
            return {
                ...state,
                user: null
            };
        case 'page-navigation':
            return {
                ...state,
                currentPage: action.currentPage
            };
        default:
            return state;
    }
};


const store =  configureStore({
    reducer:reducer,
    preloadedState: loadFromLocalStorage()
})

store.subscribe(() => {
    saveToLocalStorage(store.getState())
})

export default store;


