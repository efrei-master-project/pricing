﻿import React from "react";
import Chart from "react-apexcharts";
import {Box, Flex} from "@chakra-ui/react";


const CandleChart = ({props}) => {
    return (
        <Box >
            <Chart options={props.chart.options} series={props.chart.series} type="candlestick" />
        </Box>
    );
}

export default CandleChart;