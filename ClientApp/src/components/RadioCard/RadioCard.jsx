import {Box, useRadio} from "@chakra-ui/react";

const RadioCard = (props) => {
    const {getInputProps, getCheckboxProps} = useRadio(props)

    const input = getInputProps()
    const checkbox = getCheckboxProps()
    
    return (
        <Box as='label'>
            <input {...input} />
            <Box
                {...checkbox}
                cursor='pointer'
                borderWidth='1px'
                borderRadius='14px'
                border={0}
                _checked={{
                    bg: '#F1F1F1',
                    color: '#606166',
                    borderColor: 'teal.600',
                }}
                _focus={{
                    boxShadow: 'outline',
                }}
                fontSize={"0.8rem"}
                px={3}
                py={1}
            >
                {props.children}
            </Box>
        </Box>
    )
}

export default RadioCard;