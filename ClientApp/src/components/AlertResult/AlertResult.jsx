import {
    Box,
    Button,
    Divider,
    Flex,
    Modal,
    ModalBody,
    ModalCloseButton,
    ModalContent,
    ModalFooter,
    ModalHeader,
    ModalOverlay,
    Text
} from "@chakra-ui/react";

const AlertResult = (props) => {
    return (
        <Modal onClose={props.onClose} size={"full"} m={"2rem"} isOpen={props.isOpen}>
            <ModalOverlay/>
            <ModalContent>
                <ModalHeader>Option Price</ModalHeader>
                <ModalCloseButton/>
                <ModalBody>

                    <Flex flexDirection={"row"} justify={"space-evenly"} h={"100%"}>
                        <Flex flexDirection={"column"} h={"100%"} justify={"space-around"} gap={5} w={"40%"}>
                            <Flex flexDirection={"row"} justify={"space-between"}>
                                <Text>Call </Text>
                                <Text fontWeight={"bold"}>{props.call}</Text>
                            </Flex>
                            <Divider/>
                            <Flex flexDirection={"row"} justify={"space-between"}>
                                <Text>Number of contract </Text>
                                <Text fontWeight={"bold"}>{props.numberContract}</Text>
                            </Flex>
                            <Divider/>
                            <Text>Details :</Text>
                            <Flex ml={"1rem"} gap={2} flexDirection={"column"}  color={"#9b9a9a"}>
                                <Flex flexDirection={"row"} justify={"space-between"}>
                                    <Text>Maturity </Text>
                                    <Text fontWeight={"bold"}>{props.maturity}</Text>
                                </Flex>
                                <Flex flexDirection={"row"} justify={"space-between"}>
                                    <Text>Volatility </Text>
                                    <Text fontWeight={"bold"}>{props.volatility}</Text>
                                </Flex>
                                <Flex flexDirection={"row"} justify={"space-between"}>
                                    <Text>Risk Free Rate </Text>
                                    <Text fontWeight={"bold"}>{props.riskFreeRate}</Text>
                                </Flex>
                            </Flex>
                            <Divider/>
                            <Flex flexDirection={"row"} justify={"space-between"}>
                                <Text>Total </Text>
                                <Text fontWeight={"bold"}>{props.totalCall}</Text>
                            </Flex>

                            <Button onClick={props.buyCall} colorScheme={'blue'}>
                                Buy Call
                            </Button>
                        </Flex>
                        <Flex flexDirection={"column"} justify={"space-around"} gap={5} w={"40%"}>
                            <Flex flexDirection={"row"} justify={"space-between"}>
                                <Text>Put </Text>
                                <Text fontWeight={"bold"}>{props.put}</Text>
                            </Flex>
                            <Divider/>
                            <Flex flexDirection={"row"} justify={"space-between"}>
                                <Text>Number of contract </Text>
                                <Text fontWeight={"bold"}>{props.numberContract}</Text>
                            </Flex>
                            <Divider/>
                            <Text>Details :</Text>
                            <Flex ml={"1rem"} gap={2} flexDirection={"column"} fontSize={"0.9rem"} color={"#9b9a9a"}>
                                <Flex flexDirection={"row"} justify={"space-between"}>
                                    <Text>Maturity </Text>
                                    <Text fontWeight={"bold"}>{props.maturity}</Text>
                                </Flex>

                                <Flex flexDirection={"row"} justify={"space-between"}>
                                    <Text>Volatility </Text>
                                    <Text fontWeight={"bold"}>{props.volatility}</Text>
                                </Flex>
                                <Flex flexDirection={"row"} justify={"space-between"}>
                                    <Text>Risk Free Rate </Text>
                                    <Text fontWeight={"bold"}>{props.riskFreeRate}</Text>
                                </Flex>
                            </Flex>
                            <Divider/>
                            <Flex flexDirection={"row"} justify={"space-between"}>
                                <Text>Total </Text>
                                <Text fontWeight={"bold"}>{props.totalPut}</Text>
                            </Flex>

                            <Button onClick={props.buyPut} colorScheme={'red'}>
                                Buy Put
                            </Button>
                        </Flex>
                    </Flex>
                </ModalBody>
            </ModalContent>
        </Modal>
    )
}

export default AlertResult;