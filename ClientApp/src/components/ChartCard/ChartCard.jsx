﻿import React from "react";
import Chart from "react-apexcharts";
import {Box, Flex, Text} from "@chakra-ui/react";


const ChartCard = ({props}) => {
    return (
        <Box borderColor={"#E6E7EE"} borderStyle={"solid"} borderWidth={"1px"} p={"1.5rem"}>
            <Flex justify={"space-between"}>
                <Box fontWeight={"bold"}>{props.symbol}</Box>
                <Flex gap={"2"} alignItems={"baseline"}>
                    <Box fontWeight={"bold"} fontSize={"1.2rem"}>{props.value}</Box>
                    <Box>{props.currency}</Box>
                </Flex>
            </Flex>
            <Chart options={props.chart.options} series={props.chart.series} type="line" height={350} width={"100%"}/>

            <Flex justify={"space-between"} alignItems={"baseline"} fontWeight={"500"}>
                <Box>
                    <Text display={'inline-block'} mr={"0.2rem"}>Volume :</Text>
                    <Text display={'inline-block'} mr={"0.2rem"} color={"#CAC9CA"}>{props.volume}</Text>
                    <Text display={'inline-block'} mr={"0.2rem"}>{props.currency}</Text>
                </Box>
                <Box>{props.rate} %</Box>
            </Flex>
        </Box>
    );
}

export default ChartCard;