import React from 'react';
import SideBar from '../SideBar';

const Layout = (props) => {
    return (
        <>
            <SideBar>
                {props.children}
            </SideBar>
        </>
    );
}

export default Layout;