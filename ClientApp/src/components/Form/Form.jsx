import {
    FormControl,
    FormLabel,
    Text,
    Slider,
    SliderTrack,
    SliderFilledTrack,
    SliderThumb,
    Button,
    Box,
    Stack,
    Input,
    NumberInputField,
    NumberInput,
    NumberInputStepper,
    NumberIncrementStepper,
    NumberDecrementStepper, Flex, Grid, useDisclosure, InputRightAddon, InputGroup

} from '@chakra-ui/react'
import React, {useState} from "react";
import {MdQueryBuilder} from "react-icons/md";
import Alert from "../Alert";
import AlertResult from "../AlertResult";

const Form = (props) => {
    const symbol = props.symbol;
    const [maturity, setMaturity] = useState(2);
    const [error, setError] = useState('')
    const [numberContract, setNumberContract] = useState(1);

    const [result, setResult] = useState({});

    const {isOpen, onOpen, onClose} = useDisclosure()
    const cancelRef = React.useRef()

    const calculatePrice = async (event) => {
        event.preventDefault();
        try {
            const response = await fetch(`data/pricer?strikePrice=${props.latest}&maturityTime=${maturity}&stockName=${symbol}`);

            let data = await response.json();

            if (!response.ok) {
                throw new Error(data.Message);
            }

            data.totalCall = data.call * numberContract;
            data.totalPut = data.put * numberContract;

            setResult(data);
            onOpen()
        } catch (reason) {
            setError(reason.message)
        }

    };

    return (
        <>
            <Flex border={"#EAE8EB solid 1px"} p={"2rem"} h={"100%"}>

                <form onSubmit={calculatePrice} style={{height: "100%"}}>
                    <Flex justify={"space-around"} flexDirection={"column"} h={"100%"}>
                        <Text textAlign={"center"} fontSize='30px' fontWeight={"bold"}>
                            Option
                        </Text>
                        {error && <Alert message={error}/>}
                        <FormControl>
                            <FormLabel htmlFor='strick'>Strick Price</FormLabel>
                            <NumberInput min={10} value={props.latest}
                                         onChange={value => props.setPrice(value)}>
                                <NumberInputField id='strick'/>
                                <NumberInputStepper>
                                    <NumberIncrementStepper/>
                                    <NumberDecrementStepper/>
                                </NumberInputStepper>
                            </NumberInput>
                        </FormControl>
                        <FormControl>
                            <FormLabel>Time to maturity</FormLabel>
                            <Grid gridTemplateColumns={"2fr 1fr"} justify={"space-between"} gap={3}>
                                <Slider aria-label='slider-ex-4' value={maturity}
                                        onChange={value => setMaturity(value)}>
                                    <SliderTrack bg='blue.100'>
                                        <SliderFilledTrack bg='blue'/>
                                    </SliderTrack>
                                    <SliderThumb boxSize={6}>
                                        <Box color='blue' as={MdQueryBuilder}/>
                                    </SliderThumb>
                                </Slider>
                                <InputGroup>

                                    <NumberInput min={1} value={maturity}
                                                 onChange={value => setMaturity(value)}
                                    >
                                        <NumberInputField id='maturity'/>
                                    </NumberInput>

                                    <InputRightAddon children='Days'/>
                                </InputGroup>
                            </Grid>
                        </FormControl>

                        <FormControl>
                            <FormLabel htmlFor='simulation'>Number of contract</FormLabel>
                            <NumberInput value={numberContract} min={1}
                                         onChange={value => setNumberContract(value)}>
                                <NumberInputField id='simulation'/>
                                <NumberInputStepper>
                                    <NumberIncrementStepper/>
                                    <NumberDecrementStepper/>
                                </NumberInputStepper>
                            </NumberInput>
                        </FormControl>

                        <FormControl pt={"1rem"}>
                            <Flex>
                                <Button m={"auto"} colorScheme='blue' w={"50%"} type={"submit"}>Submit</Button>
                            </Flex>
                        </FormControl>
                    </Flex>
                </form>
            </Flex>

            <Flex p={"2rem"}>
                <AlertResult
                    isOpen={isOpen}
                    leastDestructiveRef={cancelRef}
                    onClose={onClose}
                    {...result}
                    strike={props.latest}
                    maturity={maturity}
                    numberContract={numberContract}
                />
            </Flex>
        </>
    )
};


export default Form;
