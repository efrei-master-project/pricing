import {
    Box,
    Drawer,
    DrawerContent, useColorModeValue,
    useDisclosure,
} from '@chakra-ui/react'
import React from "react";
import SidebarContent from "./SideBarContent";
import Header from "../Header";

const Sidebar = ({children}) => {
    const {isOpen, onOpen, onClose} = useDisclosure();
    return (
        <Box minH="100vh" bg={useColorModeValue('white', 'gray.900')}>
            <SidebarContent
                onClose={() => onClose}
                display={{base: 'none', md: 'block'}}
            />
            <Drawer
                autoFocus={false}
                isOpen={isOpen}
                placement="left"
                onClose={onClose}
                returnFocusOnClose={false}
                onOverlayClick={onClose}
                size="full">
                <DrawerContent>
                    <SidebarContent onClose={onClose} isOpen={isOpen}/>
                </DrawerContent>
            </Drawer>
            <Header onOpen={onOpen} title={"Test"}/>
            <Box ml={{base: 0, md: 60}}>
                {children}
            </Box>
        </Box>
    )
}

export default Sidebar