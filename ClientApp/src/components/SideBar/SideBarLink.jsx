import {Flex} from "@chakra-ui/react";
import React from "react";
import {Link} from "react-router-dom";

const SideBarLink = ({link}) => {

    return (
        <Link to={link.route} style={{width: "100%", textDecoration:"none"}}>
            <Flex p={"3"} color={"#CAC9CA"} alignItems={"center"} _hover={{"backgroundColor": "gray.300", "color": "white"}}
                  rounded={"md"} mt={"2"} gap={2} >
                {link.icon}
                {link.name}
            </Flex>
        </Link>
    )
}


export default SideBarLink;