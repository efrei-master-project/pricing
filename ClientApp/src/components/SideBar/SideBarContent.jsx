import {DashboardIcon, PriceIcon, SettingIcon, TendIcon, TransactionIcon, WalletIcon} from "../../assets/images/icons";
import {CloseButton, Flex, useColorModeValue} from "@chakra-ui/react";
import Logo from "../Logo";
import SideBarLink from "./SideBarLink";
import React from "react";
import store from "../../utils/store";

const SidebarContent = ({onClose, isOpen, ...rest}) => {
    const {user} = store.getState();

    const LinkItems = [
        [
            {route: '/', icon: <DashboardIcon/>, name: "Dashboard", show: true},
            {route: '/tends', icon: <TendIcon/>, name: "Tends", show: true},
            {route: '/pricing', icon: <PriceIcon/>, name: "Prices", show: true},
            {route: '/transactions', icon: <TransactionIcon/>, name: "Transactions", show: !!user},
            {route: '/calculator', icon: <WalletIcon/>, name: "Calculator", show: true},
        ], [
            {route: '/team', icon: <WalletIcon/>, name: "Team", show: true},
            {route: '/contact', icon: <WalletIcon/>, name: "Contact", show: true},
            {route: '/about', icon: <WalletIcon/>, name: "About", show: true},
        ]
    ];

    return (
        <Flex
            direction={"column"}
            bg={useColorModeValue('gray.100', 'gray.900')}
            borderRight="1px"
            borderRightColor={useColorModeValue('gray.200', 'gray.700')}
            w={{base: 'full', md: "min-content"}}
            pos="fixed"
            h="full"
            px={"1rem"}
            {...rest}>
            <Flex alignItems="center" h={"20"}
                  justifyContent={{base: "space-between", md: "center"}}>
                <Logo/>
                <CloseButton display={{base: 'flex', md: 'none'}} onClick={onClose}/>
            </Flex>
            {LinkItems.map((group, groupIndex) => (
                <Flex borderTop={"1px solid #CAC9CA"} direction={"column"} p={"3"} key={groupIndex}>
                    {group.filter(link => link.show).map((link, index) => (
                        <SideBarLink link={link} key={index}/>
                    ))}
                </Flex>
            ))}
        </Flex>
    );
}

export default SidebarContent;