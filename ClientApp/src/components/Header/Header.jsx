import React from 'react';
import {
    Avatar,
    Box,
    Button,
    Flex,
    HStack,
    IconButton,
    Menu,
    MenuButton,
    MenuDivider,
    MenuItem,
    MenuList,
    Text,
    useColorMode,
    useColorModeValue,
    VStack
} from "@chakra-ui/react";
import {FiMoon, FiSun, FiChevronDown, FiMenu} from "react-icons/fi";
import Logo from "../Logo";
import {Link} from "react-router-dom";
import store from '../../utils/store';
import {getAuth, signOut} from "firebase/auth";
import {useNavigate} from "react-router";

const Header = ({onOpen}) => {
    const {toggleColorMode} = useColorMode();
    const SwitchIcon = useColorModeValue(FiMoon, FiSun)
    const {user} = store.getState();
    const navigate = useNavigate();

    let photo = "https://bit.ly/broken-link";
    let username = "Not connected";

    if (user) {
        username = user.displayName ? user.displayName : user.email;
        if (user.photoUrl) photo = user.photoUrl;
    }

    const signOutCallback = async () => {
        try{
            await fetch("user/sign-out");
            store.dispatch({type: 'sign-out'});
            navigate('/')
            window.location.reload();
        }catch (error){
            console.log(error);
        }
    }

    return (
        <Flex
            ml={{base: 0, md: 60}}
            px={{base: 4, md: 4}}
            height="20"
            alignItems="center"
            bg={useColorModeValue('white', 'gray.900')}
            justifyContent={"space-between"}>
            <IconButton
                display={{base: 'flex', md: 'none'}}
                onClick={onOpen}
                variant="outline"
                aria-label="open menu"
                icon={<FiMenu/>}
            />

            <Box/>
            <Flex alignItems={"center"} display={{base: "flex", md: 'none'}}>
                <Logo maxHeight={80} maxWidth={80}/>
                <Text
                    fontSize="2xl"
                    fontFamily="heading"
                    fontWeight="bold"
                >OptChain</Text>
            </Flex>


            <Flex alignItems={'center'} gap={5}>
                <IconButton onClick={toggleColorMode} icon={<SwitchIcon/>} aria-label={"colormode"}/>
                {
                    user ?
                        <Menu>
                            <MenuButton
                                py={2}
                                transition="all 0.3s"
                                _focus={{boxShadow: 'none'}}>
                                <HStack>
                                    <Avatar
                                        size={'sm'}
                                        src={photo}
                                    />
                                    <VStack
                                        display={{base: 'none', md: 'flex'}}
                                        alignItems="flex-start"
                                        spacing="1px"
                                        ml="2">
                                        <Text fontSize="sm">
                                            {username}
                                        </Text>
                                    </VStack>
                                    <Box display={{base: 'none', md: 'flex'}}>
                                        <FiChevronDown/>
                                    </Box>
                                </HStack>
                            </MenuButton>

                            <MenuList
                                bg={useColorModeValue('white', 'gray.900')}
                                borderColor={useColorModeValue('gray.200', 'gray.700')}>
                                <MenuItem as={Link} style={{color: "inherit", textDecoration: "none"}}
                                          to={"/settings"}>Settings</MenuItem>
                                <MenuDivider/>
                                <MenuItem onClick={signOutCallback}
                                          style={{color: "inherit", textDecoration: "none"}} to={"/sign-out"}>Sign
                                    out</MenuItem>
                            </MenuList>
                        </Menu>
                        :
                        <Button as={Link} style={{color: "inherit", textDecoration: "none"}} to={"/sign-in"}>Sign
                            in</Button>
                }
            </Flex>
        </Flex>
    );
}

export default Header;