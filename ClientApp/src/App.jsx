import React from 'react';
import {Route, Routes} from 'react-router';
import Layout from './components/Layout';
import Home from './pages/Home';
import About from './pages/About';
import Team from './pages/Team';
import ContactUs from './pages/ContactUs';
import Calculator from './pages/Calculator';
import PrivacyPolicy from './pages/PrivacyPolicy';
import Transactions from './pages/Transactions';
import Settings from "./pages/Settings";
import SignIn from "./pages/SignIn";
import SignUp from "./pages/SignUp";
import Error from "./pages/Error";
import store from "./utils/store";
import Tends from "./pages/Tends";
import Pricing from "./pages/Pricing";

const App = () => {
    const {user} = store.getState();
    let otherRoutes;
    if(user){
        otherRoutes = (
            <>
                <Route exact path='/settings' element={<Settings/>}/>
                <Route exact path='/transactions' element={<Transactions/>}/>
            </>
        ) 
    }
    
    return (
        <Layout>
            <Routes>
                <Route exact path='/' element={<Home/>}/>
                <Route exact path='/tends' element={<Tends/>}/>
                <Route exact path='/sign-in' element={<SignIn/>}/>
                <Route exact path='/sign-up' element={<SignUp/>}/>
                <Route exact path='/about' element={<About/>}/>
                <Route exact path='/team' element={<Team/>}/>
                <Route exact path='/calculator' element={<Calculator/>}/>
                <Route exact path='/pricing' element={<Pricing/>}/>
                <Route exact path='/settings' element={<Settings/>}/>
                <Route exact path='/contact' element={<ContactUs/>}/>
                <Route exact path='/privacy-policy' element={<PrivacyPolicy/>}/>
                {otherRoutes}
                <Route exact path='/*' element={<Error/>}/>
            </Routes>
        </Layout>
    );
    
}

export default App;
