import CalculatorIcon from "./CalculatorIcon";
import DashboardIcon from "./DashboardIcon";
import PriceIcon from "./PriceIcon";
import SettingIcon from "./SettingIcon";
import TendIcon from "./TendIcon";
import TransactionIcon from "./TransactionIcon";
import WalletIcon from "./WalletIcon";


export {WalletIcon, TransactionIcon, TendIcon, SettingIcon, PriceIcon, DashboardIcon, CalculatorIcon};