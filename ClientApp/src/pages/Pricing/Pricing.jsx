import React, {useEffect, useState} from 'react';
import {
    Alert,
    AlertDescription,
    AlertIcon,
    AlertTitle,
    Box,
    Flex,
    Grid,
    Select,
    Text,
    useRadioGroup
} from '@chakra-ui/react'
import CandleChart from "../../components/CandleChart/CandleChart.jsx"
import Form from "../../components/Form"
import RadioCard from "../../components/RadioCard";


const Pricing = () => {
    const [priceData, setPriceData] = useState([]);
    const symbols = [
        "MSFT", "AAPL", "AMZN", "GOOG", "BABA", "BRK.B", "VOD", "JPM"
    ];

    const [symbol, setSymbol] = useState(symbols[0]);
    const gaps = [
        {key: "1m", gap: 1, unit: "month"},
        {key: "3m", gap: 3, unit: "month"},
        {key: "6m", gap: 6, unit: "month"},
        {key: "1y", gap: 1, unit: "year"},
    ];

    const [gap, setGap] = useState(1);
    const [latest, setLatest] = useState(0);
    const [unit, setUnit] = useState("month");
    const [error, setError] = useState(null);

    const {getRootProps, getRadioProps} = useRadioGroup({
        name: 'gaps',
        defaultValue: gaps[0].key,
        onChange: async (value) => {
            const gapObject = gaps.find(element => element.key === value);
            setGap(gapObject.gap);
            setUnit(gapObject.unit);

            await fetchData();
        },
    });

    const fetchData = async () => {
        console.log(gap, unit);
        const response = await fetch(`data/pricing-data?gap=${gap}&unit=${unit}&symbol=${symbol}`)
        const data = await response.json();

        if (!response.ok) {
            setError("An error occured, please retry later");
            console.warn(data.Message);
            return;
        }
        console.log(data[data.length-1].close)
        setLatest(data.length > 0 ? data[data.length-1].close : 0);
        
        setPriceData(data)
    }

    const updateSymbol = async (event) => {
        setSymbol(event.target.value);

        await fetchData();
    }


    useEffect(() => {
        if (data.length === 0) {
            fetchData();
        }

    }, []);

    const data = priceData.map((element, index) => {
        const date = new Date(element.date).toLocaleDateString("fr");
        return {
            x: date,
            y: [element.open, element.high, element.low, element.close]
        }

    })
    const prop =
        {
            chart: {
                options: {
                    chart: {
                        height: 350,
                        type: 'candlestick',
                        zoom: {
                            enabled: true
                        },
                    },
                    dataLabels: {
                        enabled: false
                    },
                    plotOptions: {
                        candlestick: {
                            colors: {
                                upward: '#57C8EC',
                                downward: '#F04E7F'
                            }
                        },
                    },
                    xaxis: {
                        categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep'],
                    }
                },
                series: [{
                    data: data.reverse(),
                }],

            }
        };

    return (

        <Grid gridTemplateColumns={"2fr 1fr"} alignItems={"center"} p={"2rem"} gap={"5"}>
            <Box>
                {
                    error !== null ?
                        <Alert status='error' m={"auto"} w={"50%"}>
                            <AlertIcon/>
                            <AlertTitle>Error</AlertTitle>
                            <AlertDescription>{error}</AlertDescription>
                        </Alert>
                        :
                        <Box border={"#EAE8EB solid 1px"} p={"2rem"}>
                            <Select w={"max-content"} mb={"1rem"} onChange={updateSymbol}>
                                {
                                    symbols.map((symbol, index) => {
                                        return (
                                            <option value={symbol} key={index}>{symbol}</option>
                                        )
                                    })
                                }
                            </Select>
                            <Flex gap={1}>
                                {gaps.map((gapSettings, index) => {
                                    const radio = getRadioProps({value: gapSettings.key});

                                    return (
                                        <RadioCard key={index} {...radio}>
                                            {gapSettings.key}
                                        </RadioCard>
                                    )
                                })}
                            </Flex>
                            <CandleChart props={prop}/>
                        </Box>
                }
            </Box>
            <Form symbol={symbol} latest={latest} setPrice={setLatest}/>
        </Grid>

    );
}

export default Pricing;