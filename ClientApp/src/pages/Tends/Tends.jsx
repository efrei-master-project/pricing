import React, {useEffect, useState} from "react";
import {Alert, AlertDescription, AlertIcon, AlertTitle, Box, Grid, Heading, Spinner} from "@chakra-ui/react";
import ChartCard from "../../components/ChartCard";

const Tends = () => {

    const [data, setData] = useState([]);
    const [loading, setLoading] = useState(true);
    const [error, setError] = useState(null);
    const symbols = [
        "MSFT", "AAPL", "AMZN", "GOOG", "BABA", "BRK.B", "VOD", "JPM"
    ];

    useEffect(() => {
        const initData = async () => {
            const queryParameters = symbols.join("&symbols=");
            const response = await fetch('data/tends?symbols=' + queryParameters);
            const data = await response.json();
            if (!response.ok) {
                setError("An error occured, please retry later");
                console.warn(data.Message);
                setLoading(false);
                return;
            }

            const chart = [];
            let index = 0;

            for (const datum of Object.values(data)) {

                const closes = datum.map((element) => element.close);
                const formatNumber = (number) => number < 10 ? "0" + number : number;
                const axis = datum.map((element) => {
                    const date = new Date(element.date);
                    return `${formatNumber(date.getMonth())}/${formatNumber(date.getDate())}`
                });
                let colors = "#57C8EC"
                let symbol = symbols[index];
                let value = 0;
                let rate = 0;
                let volume = 0;

                if (datum.length > 0) {
                    var n2 = closes[closes.length - 2];
                    var n1 = closes[closes.length - 1]
                    colors = n1 < n2 ? "#F04E7F" : "#57C8EC";
                    symbol = datum[0].symbol;
                    value = n1;
                    rate = Math.round(((n1 - n2) / n2 * 100) * 100) / 100;
                    rate = rate > 0 ? `+${rate}` : rate;
                    volume = datum[datum.length - 1].volume
                }


                chart.push({
                    value: value,
                    symbol: symbol,
                    rate: rate,
                    volume: volume,
                    currency: "USD",
                    chart: {
                        series: [{
                            name: symbol,
                            data: closes
                        }],
                        options: {
                            colors: [colors],
                            chart: {
                                height: 200,
                                type: 'line',
                                zoom: {
                                    enabled: true
                                }
                            },
                            xaxis: {
                                categories: axis,
                            }
                        }
                    }
                })
                index++;
            }

            setData(chart);
            setLoading(false);
        }

        if (data.length === 0) {
            initData();
        }
    });

    return (
        <Box>
            <Heading mb={"1rem"}>Tends</Heading>
            <Box display={"flex"} pr={"3rem"}>
                {
                    loading ?
                        <Spinner size='xl' thickness='4px' m={"auto"}/>
                        : <></>
                }
                {

                    error !== null ?
                        <Alert status='error' m={"auto"} w={"50%"}>
                            <AlertIcon/>
                            <AlertTitle>Error</AlertTitle>
                            <AlertDescription>{error}</AlertDescription>
                        </Alert>
                        :
                        <Grid templateColumns='repeat(3, 1fr)' w={"100%"} gap={"1rem"}>
                            {data.map((element, index) => {
                                return (
                                    <Box m={"auto"} key={index} w={"100%"}>
                                        <ChartCard key={index} props={element}/>
                                    </Box>
                                )
                            })
                            }
                        </Grid>
                }
            </Box></Box>
    );
}

export default Tends