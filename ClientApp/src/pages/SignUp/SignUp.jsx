import {Button, FormControl, FormLabel, Input, Flex, Box, Text} from "@chakra-ui/react";
import {useState} from "react";
import {getAuth, createUserWithEmailAndPassword, sendEmailVerification} from "firebase/auth";
import Alert from '../../components/Alert';
import {useNavigate} from "react-router";
import {Link} from "react-router-dom";

const SignUp = () => {
    const [isSubmitting, setIsSubmitting] = useState(false)
    const [email, setEmail] = useState('')
    const [error, setError] = useState('')
    const [password, setPassword] = useState('')
    const [confirmPassword, setConfirmPassword] = useState('')
    const isError = password !== confirmPassword
    const navigate = useNavigate();
    
    return (
        <Flex>
            <Box margin={"auto"} border={"1px solid"} borderColor={" gray.300"} borderRadius={"10px"} padding={10}>
            <form onSubmit={event => {
                event.preventDefault();
                setIsSubmitting(true);
                createUserWithEmailAndPassword(getAuth(), email, password)
                    .then((credentials) => {
                        setIsSubmitting(false);
                        sendEmailVerification(credentials.user)
                        navigate('/dashboard');
                    }).catch(reason => {
                    setIsSubmitting(false);
                    setError(reason.message)
                });
            }}>
                {error && <Alert message={error}/>}
                <FormControl>
                    <FormLabel htmlFor='email'>Email address</FormLabel>
                    <Input id='email' type='email' required onChange={event => setEmail(event.currentTarget.value)}/>
                </FormControl>
                <FormControl isInvalid={isError}>
                    <FormLabel htmlFor='password'>Password</FormLabel>
                    <Input id='password' type='password' required onChange={event => setPassword(event.currentTarget.value)}/>
                </FormControl>
                <FormControl isInvalid={isError}>
                    <FormLabel htmlFor='confirmPassword'>Confirm Password</FormLabel>
                    <Input id='confirmPassword' type='password' required onChange={event => setConfirmPassword(event.currentTarget.value)}/>
                </FormControl>
                <Text mt={3}>You already have an account ? Please <Link to="/sign-in" style={{"color":'var(--chakra-colors-teal-500)'}}>sign in</Link></Text>
                <Button
                    mt={4}
                    colorScheme='teal'
                    isLoading={isSubmitting}
                    type='submit'
                >
                    Submit
                </Button>
            </form>
            </Box>
        </Flex>
    )
}


export default SignUp;