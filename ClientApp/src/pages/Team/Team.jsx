import React from 'react';
import {Badge, Box, Image, Flex, Grid, Heading, useColorModeValue} from "@chakra-ui/react";
import martin from "../../assets/images/Photos/PPMartin.png";
import marine from "../../assets/images/Photos/PPMarine.jpg";
import guillaume from "../../assets/images/Photos/PPGuillaume.jpeg";
import maxime from "../../assets/images/Photos/PPMaxime.jpeg";
import yann from "../../assets/images/Photos/PPYann.jpeg";
import antoine from "../../assets/images/Photos/PPAntoine.jpg";
import marin from "../../assets/images/Photos/PPMarin.jpeg";

const Team = () => {
    
    return (
        <>
            <Heading style={{textAlign: "center"}}>The Optchain Team</Heading>
            <br/>
        <Grid gridTemplateColumns={'repeat(4,1fr)'} gap={5}  style = {{marginRight: '20px'}} >
            
        <Box maxW='sm' borderWidth='1px' borderRadius='lg' overflow='hidden' style={{textAlign: "center"}} >
            
            <Image src={guillaume} />

            <Box p='6'>
                <Box display='flex' alignItems='baseline'>
                    <Box
                        color='gray.500'
                        fontWeight='semibold'
                        letterSpacing='wide'
                        fontSize='xs'
                        textTransform='uppercase'
                        ml='2'
                    >
                    </Box>
                </Box>

                <Box
                    mt='1'
                    fontWeight='semibold'
                    as='h4'
                    lineHeight='tight'
                    noOfLines={1}
                >
                </Box>

                <Box>
                    <Box as='span'  fontSize='sm' color={useColorModeValue('gray.600', 'light')}>
                        Guillaume André
                    </Box>
                </Box>
                <Box as='span' color={useColorModeValue('gray.600', 'light')} fontSize='sm'>
                    Business
                </Box>
            </Box>
        </Box>
            <Box maxW='sm' borderWidth='1px' borderRadius='lg' overflow='hidden' style={{textAlign: "center"}} >

                <Image src={marine} />

                <Box p='6'>
                    <Box display='flex' alignItems='baseline'>
                        <Box
                            color='gray.500'
                            fontWeight='semibold'
                            letterSpacing='wide'
                            fontSize='xs'
                            textTransform='uppercase'
                            ml='2'
                        >
                        </Box>
                    </Box>

                    <Box
                        mt='1'
                        fontWeight='semibold'
                        as='h4'
                        lineHeight='tight'
                        noOfLines={1}
                    >
                    </Box>

                    <Box>
                        <Box as='span' color={useColorModeValue('gray.600', 'light')} fontSize='sm'>
                            Marine Ango
                        </Box>
                    </Box>
                    <Box as='span' color={useColorModeValue('gray.600', 'light')} fontSize='sm'>
                        Developpeuse front 
                    </Box>
                </Box>
            </Box>
            <Box maxW='sm' borderWidth='1px' borderRadius='lg' overflow='hidden' style={{textAlign: "center"}} >

                <Image src={maxime} />

                <Box p='6'>
                    <Box display='flex' alignItems='baseline'>
                        <Box
                            color='gray.500'
                            fontWeight='semibold'
                            letterSpacing='wide'
                            fontSize='xs'
                            textTransform='uppercase'
                            ml='2'
                        >
                        </Box>
                    </Box>

                    <Box
                        mt='1'
                        fontWeight='semibold'
                        as='h4'
                        lineHeight='tight'
                        noOfLines={1}
                    >
                    </Box>

                    <Box>
                        <Box as='span' color={useColorModeValue('gray.600', 'light')} fontSize='sm'>
                            Maxime Bertier
                        </Box>
                    </Box>
                    <Box as='span' color={useColorModeValue('gray.600', 'light')} fontSize='sm'>
                        Developpeur Csharp
                    </Box>
                </Box>
            </Box>
            <Box maxW='sm' borderWidth='1px' borderRadius='lg' overflow='hidden' style={{textAlign: "center"}} >

                <Image src={martin} />

                <Box p='6'>
                    <Box display='flex' alignItems='baseline'>
                        <Box
                            color='gray.500'
                            fontWeight='semibold'
                            letterSpacing='wide'
                            fontSize='xs'
                            textTransform='uppercase'
                            ml='2'
                        >
                        </Box>
                    </Box>

                    <Box
                        mt='1'
                        fontWeight='semibold'
                        as='h4'
                        lineHeight='tight'
                        noOfLines={1}
                    >
                    </Box>

                    <Box>
                        <Box as='span' color={useColorModeValue('gray.600', 'light')} fontSize='sm'>
                           Isaac Charaf
                        </Box>
                    </Box>
                    <Box as='span' color={useColorModeValue('gray.600', 'light')} fontSize='sm'>
                        Business
                    </Box>
                </Box>
            </Box>
            <Box maxW='sm' borderWidth='1px' borderRadius='lg' overflow='hidden' style={{textAlign: "center"}} >

                <Image src={martin} />

                <Box p='6'>
                    <Box display='flex' alignItems='baseline'>
                        <Box
                            color='gray.500'
                            fontWeight='semibold'
                            letterSpacing='wide'
                            fontSize='xs'
                            textTransform='uppercase'
                            ml='2'
                        >
                        </Box>
                    </Box>

                    <Box
                        mt='1'
                        fontWeight='semibold'
                        as='h4'
                        lineHeight='tight'
                        noOfLines={1}
                    >
                    </Box>

                    <Box>
                        <Box as='span' color={useColorModeValue('gray.600', 'light')} fontSize='sm'>
                            Martin Lenaerts
                        </Box>
                    </Box>
                    <Box as='span' color={useColorModeValue('gray.600', 'light')} fontSize='sm'>
                        CTO
                    </Box>
                </Box>
            </Box>
            <Box maxW='sm' borderWidth='1px' borderRadius='lg' overflow='hidden' style={{textAlign: "center"}}>

                <Image src={yann} />

                <Box p='6'>
                    <Box display='flex' alignItems='baseline'>
                        <Box
                            color='gray.500'
                            fontWeight='semibold'
                            letterSpacing='wide'
                            fontSize='xs'
                            textTransform='uppercase'
                            ml='2'
                        >
                        </Box>
                    </Box>

                    <Box
                        mt='1'
                        fontWeight='semibold'
                        as='h4'
                        lineHeight='tight'
                        noOfLines={1}
                    >
                    </Box>

                    <Box>
                        <Box as='span' color={useColorModeValue('gray.600', 'light')} fontSize='sm'>
                            Yann Raymond
                        </Box>
                    </Box>
                    <Box as='span' color={useColorModeValue('gray.600', 'light')} fontSize='sm'>
                        Developpeur Back & front
                    </Box>
                </Box>
            </Box>
            <Box maxW='sm' borderWidth='1px' borderRadius='lg' overflow='hidden' style={{textAlign: "center"}}>

                <Image src={antoine} />

                <Box p='6'>
                    <Box display='flex' alignItems='baseline'>
                        <Box
                            color='gray.500'
                            fontWeight='semibold'
                            letterSpacing='wide'
                            fontSize='xs'
                            textTransform='uppercase'
                            ml='2'
                        >
                        </Box>
                    </Box>

                    <Box
                        mt='1'
                        fontWeight='semibold'
                        as='h4'
                        lineHeight='tight'
                        noOfLines={1}
                    >
                    </Box>

                    <Box>
                        <Box as='span' color={useColorModeValue('gray.600', 'light')} fontSize='sm'>
                            Antoine Thielin
                        </Box>
                    </Box>
                    <Box as='span' color={useColorModeValue('gray.600', 'light')} fontSize='sm'>
                        Developpeur Blockchain 
                    </Box>
                </Box>
            </Box>
            <Box maxW='sm' borderWidth='1px' borderRadius='lg' overflow='hidden' style={{textAlign: "center" }} >

                <Image src={marin} />

                <Box p='6'>
                    <Box display='flex' alignItems='baseline'>
                        <Box
                            color='gray.500'
                            fontWeight='semibold'
                            letterSpacing='wide'
                            fontSize='xs'
                            textTransform='uppercase'
                            ml='2'
                        >
                        </Box>
                    </Box>

                    <Box
                        mt='1'
                        fontWeight='semibold'
                        as='h4'
                        lineHeight='tight'
                        noOfLines={1}
                    >
                    </Box>

                    <Box>
                        <Box as='span' color={useColorModeValue('gray.600', 'light')} fontSize='sm'>
                            Marin Van-Remoortel
                        </Box>
                    </Box>
                    <Box as='span' color={useColorModeValue('gray.600', 'light')} fontSize='sm'>
                        Developpeur
                    </Box>
                </Box>
            </Box>
        </Grid>
        </>
        );
    
}

export default Team;