import React from 'react';
import {Box, Heading, Stack, Text} from "@chakra-ui/react";

const About = () => {

    return (

        <>
            <Heading style={{textAlign: "center"}}>About Us</Heading><br></br>

            <Heading style={{textAlign: ""}} as="h6" size="sm">

            </Heading>
            <br></br>
            <Stack spacing={3}>
                <Text fontSize='md' >
                    Vous êtes-vous déjà demandé qui à le contrôle sur la bourse ?<br></br>
                    A l’heure actuelle, l’AMF est le régulateur et législateur des acteurs de marché.<br></br><br></br>

                    Pourquoi une autorité aurait le contrôle sur tous les marchée ? <br></br>
                    Nous, nous avons remis en question le principe de ce régulateur et nous nous sommes alors demander comment permettre une régulation décentralisée des marchées financier ?

                    <br></br><br></br>
                    Avant t’étaler notre solution, Qui sommes nous ?<br></br>
                    Nous sommes une équipe de huit jeunes étudiants en master 1 et nous réalisons se projet dans le cadre de notre formation d’ingénieur.
                    Notre équipe se compose de sept membres dans la majeure ITF et un membre dans la majeure software ingineering.
                    Nous sommes : Marine, Yann, Martin, Antoine, Guillaume, Issac, mMxime et Marin (voir page Team).<br></br><br></br>

                    Aujourd’hui nous vous proposons la solution Optchain !<br></br>

                    Optchain est une platforme simple d’utilisation qui va permettre aux investisseurs comme vous d’avoir l’expérience la plus transparente possible.<br></br>

                    Cette transparance nous la devons à l’utilisation de la technologie blockchain. Que ce soit sur les transactions ou sur les commissions vous saurez tout ! <br></br>

                    Vous pourrez alors pricer une option selon vos critères et l’exercer sur le marché si vous le souhaiter mais ce n’est pas tout !<br></br>
                    <br></br>
                    Notre platforme vous permettra de visualiser une page des tendances du marché, d’avoir un appercu de votre wallet ainsi qu’une page de calcul.
                </Text>
            </Stack>
        </>
    );
}

export default About;