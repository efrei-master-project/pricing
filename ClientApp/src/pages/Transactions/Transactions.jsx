import React from 'react';
import {Box,SimpleGrid,Heading, Table,
    Thead,
    Tbody,
    Tfoot,
    Tr,
    Th,
    Td,
    TableCaption,
    TableContainer,Button} from "@chakra-ui/react";

const Transactions = () => {
    
    return (
        <>
        <Heading>Transactions</Heading>
            <br></br>
            <TableContainer>
                <Table variant='simple'>
                    <TableCaption>Table summarizing transactions </TableCaption>
                    <Thead>
                        <Tr>
                            <Th>Dates</Th>
                            <Th>Transaction code</Th>
                            <Th>Price</Th>
                            <Th>Status</Th>
                        </Tr>
                    </Thead>
                    <Tbody>
                        <Tr>
                            <Td>16:23, 12 dec 2018</Td>
                            <Td>1Cs4wu6pu5qCZ35bSLNVzGyEx5N6uzbg9t</Td>
                            <Td>0.0094 LTC</Td>
                            <Td color={"blue.300"}>Complete</Td>
                        </Tr>
                        <Tr>
                            <Td>09:49, 11 dec 2018</Td>
                            <Td>1PRj85hu9RXPZTzxtko9stfs6nRo1vyrQB</Td>
                            <Td>0.4509 BTC</Td>
                            <Td color={"violet"}>Pending</Td>
                        </Tr>
                        <Tr>
                            <Td>04:08, 11 dec 2018</Td>
                            <Td>1AGm6Jc43FUaYRKm8wj2cBpJ7FWgjxwCXW</Td>
                            <Td>5.456 DASH</Td>
                            <Td color={"blue.300"}>Complete</Td>
                        </Tr>
                        <Tr>
                            <Td>22:56, 10 dec 2018</Td>
                            <Td>1P9RQEr2XeE3PEb44ZE35sfZRRW1JHU8qx</Td>
                            <Td>0.6774 ETH</Td>
                            <Td color={"blue.300"}>Complete</Td>
                        </Tr>
                        <Tr>
                            <Td>07:27, 09 dec 2018</Td>
                            <Td>45RQEr2XeE3PEb44ZE35sfZRRewwtr224s1</Td>
                            <Td>0.3 XRP</Td>
                            <Td color={"blue.300"}>Complete</Td>
                        </Tr>
                        
                    </Tbody>
                    <Tfoot>
                        <Tr>
                            <Th></Th>
                            <Th></Th>
                            <Th></Th>
                        </Tr>
                    </Tfoot>
                </Table>
            </TableContainer>
        </>
        
    );
}

export default Transactions;