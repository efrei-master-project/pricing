import React from 'react';
import {Box, Heading, Text, Stack, Divider} from "@chakra-ui/react";

const PrivacyPolicy = () => {
    
    return (
        <>
        <Heading style={{textAlign: "center"}}>Privacy Policy & Terms of Use</Heading><br></br>

            <Heading style={{textAlign: ""}} as="h6" size="sm">
                Options
            </Heading>
            <br></br>
            <Stack spacing={3}>
                <Text fontSize='md' >
                    Options are financial products known as "derivatives" that give you the right to buy or sell a quantity of an underlying asset (stock, currency, etc.) during a predetermined period and at a predetermined price. You pay a premium (option price) to acquire this right. Beware, options are risky speculative products. The point here.<br></br><br></br>

                    Options are risky speculative products. In no case do they meet medium or long term investment needs. Do not invest if :
                    <br></br>
                    •	you don't want to take a risk on your savings,<br></br>
                    •	you do not understand how these products work.<br></br><br></br>
                    
                    If you invest :<br></br>
                    •	only invest a small part of your savings (no more than 5%),<br></br>
                    •	invest in these products only on websites authorized in France, by checking their approval and authorization on the REGAFI website (register of financial agents).<br></br><br></br>

                    The first sum to pay will be the option price. It corresponds to the premium to be paid in exchange for the right to sell or buy the underlying assets.<br></br>
                    If you subsequently decide to exercise your option, you will pay the strike price, which is the price of the underlying asset agreed upon in the contract. It is firm and cannot be changed.
                    You will be charged a fee for trading options. These fees vary from one financial intermediary to another.

                </Text>
            </Stack>
            <br></br><br></br>
            <Heading style={{textAlign: ""}} as="h6" size="sm">
                RGDP
            </Heading>
            <br></br>
            <Heading style={{textAlign: ""}} as="h1" size="sm">
                ARTICLE 1. IDENTITY OF THE DATA CONTROLLER
            </Heading>
            <br></br>
            <Stack spacing={3}>
                <Text fontSize='md'>
                    Your personal data (hereinafter "Data") are collected and processed by the company OPTCHAIN, registered in the PARIS Trade and Companies Register under the number _________, and whose registered office is located _______________, France.<br></br>
                    As the controller, OPTCHAIN makes every effort to protect your privacy as much as possible and to ensure the security of your Data in accordance with the regulations in force, in particular Regulation No. 2016/679 of the European Parliament and of the Council on the protection of individuals with regard to the processing of personal data and on the free movement of such data dated 27 April 2016.<br></br>
                    Below you will find OPTCHAIN's policy on Data protection.<br></br>

                </Text>    
            </Stack>
            <br></br>
            <Heading style={{textAlign: ""}} as="h1" size="sm">
                ARTICLE 2. COLLECTION OF YOUR DATA
            </Heading>
            <br></br>
            <Stack spacing={3}>
                <Text fontSize='md'>
                    Your Data are collected from the site of the optchain.com order passage (hereinafter "Site"), by its customer service as well as on the pages published by OPTCHAIN on the social networks.<br></br>
                    OPTCHAIN collects your Data during the following operations:<br></br>

                    •	browsing on the Site through cookies deposited on your terminal;<br></br>
                    •	purchase on the Site;<br></br>
                    •	creation of a personal account on the Site;<br></br>
                    •	subscribing to the OPTCHAIN newsletter;<br></br>
                    •	exchange with OPTCHAIN's customer service;<br></br>
                    •	browsing and subscribing to the pages published by OPTCHAIN on social networks;<br></br>

                </Text>
            </Stack>
            <br></br>
            <Heading style={{textAlign: ""}} as="h1" size="sm">
                ARTICLE 3. IDENTIFICATION OF THE PROCESSED DATA
            </Heading>
            <br></br>
            <Stack spacing={3}>
                <Text fontSize='md'>
                    The Data collected includes mainly your name, first name, gender, date of birth, postal and e-mail addresses, telephone number, login and password of your personal account as well as your banking information. Some Data are mandatory, others are optional.<br></br>
                    OPTCHAIN may also process Data relating to: <br></br>

                    •	your browsing on the Site and the pages published by OPTCHAIN on social networks, namely your IP address, the pages visited, the frequency of your visits;<br></br>
                    •	payment and all banking transactions;<br></br>
                    •	your purchase history, searches and backups of financial products;<br></br>
                    •	your comments and other contributions published on the Site or the pages published by OPTCHAIN;<br></br>

                </Text>
            </Stack>
            <br></br>
            <Heading style={{textAlign: ""}} as="h1" size="sm">
                ARTICLE 4. PURPOSES OF PROCESSING YOUR DATA
            </Heading>
            <br></br>
            <Stack spacing={3}>
                <Text fontSize='md'>
                    The Data collected may be used for the following purposes:<br></br>

                    •	the processing and delivery of orders placed on the Site as well as the resulting after-sales service;<br></br>
                    •	the management of customer relations and the personalization of OPTCHAIN's communications;<br></br>
                    •	to improve and personalize OPTCHAIN's offers;
                    •	sending the OPTCHAIN newsletter and other information about products and services offered by OPTCHAIN<br></br>
                    •	the realization of commercial prospecting operations and the elaboration of marketing tools;<br></br>
                    •	the realization of technical operations linked to the above mentioned purposes;<br></br>
                    •	to comply with the legal and regulatory obligations to which OPTCHAIN is subject.<br></br>

                </Text>
            </Stack>
            <br></br>
            <Heading style={{textAlign: ""}} as="h1" size="sm">
                ARTICLE 5. LEGAL BASIS FOR THE PROCESSING CARRIED OUT
            </Heading>
            <br></br>
            <Stack spacing={3}>
                <Text fontSize='md'>
                    In accordance with the regulation in force, the processing of your Data is founded on a specific legal basis, namely:<br></br>

                    •	your express consent that OPTCHAIN will have obtained in order, on the one hand, to send you its newsletter and/or inform you about OPTCHAIN news via social networks, and on the other hand, to communicate some of your Data to certain service providers or subcontractors;<br></br>
                    •	compliance with a legal obligation that makes the processing carried out by OPTCHAIN necessary, such as, for example, the decree n°2011-219 of February 25, 2011 relating to the conservation and communication of Data allowing the identification of any person having contributed to the creation of a content put online;<br></br>
                    •	the existence of a legitimate interest for OPTCHAIN to process your Data in order to optimally manage its relations with the public and promote its products.<br></br>

                </Text>
            </Stack>
            <br></br>
            <Heading style={{textAlign: ""}} as="h1" size="sm">
                ARTICLE 6. RETENTION PERIOD OF YOUR DATA
            </Heading>
            <br></br>
            <Stack spacing={3}>
                <Text fontSize='md'>
                    The duration of the retention of your Data will be determined according to the purposes and legal obligations of OPTCHAIN.<br></br><br></br>

                    Your Data collected when you register for the OPTCHAIN newsletter, subscribe to the pages published by OPTCHAIN on social networks or create a personal account on the Site will be kept for the duration of said registration, subscription or existence of said personal account. Following any unsubscription or deletion of the personal account, your Data may be kept and processed for a period of three years. After this period, your Data will either be deleted or anonymized.<br></br><br></br>

                    Your Data collected when you place an order on the Site will be kept and processed for a period of three (3) years. After this period, your Data will either be deleted or anonymized.<br></br><br></br>

                    Your Data collected during your browsing on the Site and on the pages published by OPTCHAIN on social networks will be kept and processed for a period of thirteen months. After this period, your Data will either be deleted or anonymized. However, at the end of the aforementioned periods, including as necessary from the date of your request for deletion, your Data may be subject to intermediate archiving in order to meet the legal and regulatory obligations to which OPTCHAIN is subject.<br></br>
                </Text>
            </Stack>
            <br></br>
            <Heading style={{textAlign: ""}} as="h1" size="sm">
                ARTICLE 7. IDENTIFICATION OF RECIPIENTS OF YOUR DATA
            </Heading>
            <br></br>
            <Stack spacing={3}>
                <Text fontSize='md'>
                    All your Data is strictly confidential so that OPTCHAIN will not transmit it to any third party likely to use it for personal purposes, without your express consent. However, your Data may be transferred, in a secure manner and for the purposes mentioned above, to service providers and subcontractors used by OPTCHAIN for the processing and delivery of orders or the provision of IT, marketing or advertising services. OPTCHAIN undertakes to take all necessary measures to carry out such transfers in accordance with the regulations in force. In this respect, OPTCHAIN specifies that some of its service providers may have recourse to subcontractors located in countries outside the European Union, in particular in the United States, and undertakes to put in place all the necessary guarantees so as to manage these transfers in compliance with the regulations in force. Accordingly, you expressly authorize OPTCHAIN and its service providers to transfer your Data outside the European Union. In addition, your Data may be communicated to respond to an injunction of the judicial or administrative authorities.
                </Text>
            </Stack>
            <br></br>
            <Heading style={{textAlign: ""}} as="h1" size="sm">
                ARTICLE 8. YOUR RIGHTS
            </Heading>
            <br></br>
            <Stack spacing={3}>
                <Text fontSize='md'>
                    In accordance with the regulations in force, you have the right :<br></br>

                    •	to withdraw your consent;<br></br>
                    •	to request access, rectification or deletion of all or part of your Data;<br></br>
                    •	to limit a treatment;<br></br>
                    •	to object to processing;<br></br>
                    •	to the portability of your Data.<br></br><br></br>

                    You may exercise your rights by sending a written request, with proof of your identity, by e-mail to dpo@optchain.com. A reply will be sent to you within one month of receipt of your request. If necessary, this period may be extended by two months by OPTCHAIN who will inform you in writing.<br></br><br></br>

                    In the event that OPTCHAIN does not succeed in responding to your requests in a manner that satisfies you, you may file a complaint with the Commission Nationale de l'Informatique et des Libertés (CNIL), located at 3 Place de Fontenoy - TSA 80715 - 75334 PARIS CEDEX 07. Tel : 01.53.73.22.22.<br></br><br></br>

                    1.	You have the right to withdraw your consent, at any time, in cases where it has been requested. However, such withdrawal will not compromise the lawfulness of the processing based on the consent given prior to such withdrawal.<br></br><br></br>

                    2.	You have the right to obtain from OPTCHAIN confirmation as to whether or not your Data is being processed and, where it is, to have access to your Data as well as the following information:<br></br>

                    •	the purposes of the processing;<br></br>
                    •	the categories of Data processed;<br></br>
                    •	the recipients or categories of recipients to whom your Data has been or will be communicated;<br></br>
                    •	where possible, the period for which your Data is to be kept or, where this is not possible, the criteria used to determine this period;<br></br>
                    •	the existence of the right to request from OPTCHAIN the rectification or deletion of all or part of your Data or a limitation of the processing of your Data or the right to object to such processing;<br></br>
                    •	the right to lodge a complaint with a supervisory authority;<br></br>
                    •	where your Data has not been collected from the data subject, any available information as to its source;<br></br>
                    •	the existence of automated decision-making, including profiling, and, at least in such cases, relevant information about the underlying logic and the significance and intended consequences of such processing for you.<br></br><br></br>
                    
                    Where your Data is transferred to a third country or to an international organization, you have the right to be informed of the appropriate safeguards, with respect to such transfer. OPTCHAIN will provide a copy of your Data being processed and may charge a reasonable fee based on administrative costs for any additional copies requested. When you submit your request electronically, the information will be provided in a commonly used electronic format, unless you request otherwise.<br></br><br></br>
                    
                    3.	You have the right to have OPTCHAIN promptly correct any of your Data that is inaccurate. You also have the right to have incomplete Data completed, including by providing a supplementary declaration.<br></br><br></br>

                    4.	You have the right to obtain from OPTCHAIN the deletion, as soon as possible, of your Data when one of the following reasons applies:<br></br>

                    •	your Data is no longer necessary for the purposes for which it was collected or otherwise processed;<br></br>
                    •	you withdraw your consent on which the processing is based and there is no other legal basis for the processing;<br></br>
                    •	you object to the processing under the conditions in point 6 and there is no compelling legitimate reason for the processing;<br></br>
                    •	your Data has been processed unlawfully;<br></br>
                    •	your Data must be deleted to comply with a legal obligation;<br></br>
                    •	your Data has been collected from a child.<br></br><br></br>

                    However, this right to erasure does not apply when the processing is necessary:<br></br>

                    •	to exercise the right to freedom of expression and information ;<br></br>
                    •	to comply with a legal obligation which requires such processing or to perform a task carried out in the public interest or in the exercise of official authority vested in the controller;<br></br>
                    •	for reasons of public interest in the field of public health;<br></br>
                    •	for archival purposes in the public interest, for scientific or historical research or for statistical purposes;<br></br>
                    •	for the establishment, exercise or defence of legal claims<br></br><br></br>

                    5.	You have the right to obtain from OPTCHAIN the limitation of processing where any of the following apply:<br></br>

                    •	the time for OPTCHAIN to verify the accuracy of your Data following a challenge by you;<br></br>
                    •	the processing being unlawful, you object to the erasure of the Data and demand instead the limitation of their use;<br></br>
                    •	OPTCHAIN no longer needs your Data for the purposes of processing, but it is still necessary for the establishment, exercise or defence of legal claims;<br></br>
                    •	the time of the verification as to whether the legitimate reasons pursued by OPTCHAIN prevail over yours following an objection from you under the conditions in point 6.<br></br><br></br>

                    Where processing has been restricted, your Data may, with the exception of storage, only be processed (i) with your consent or (ii) for the establishment, exercise or defence of legal claims or (iii) for the protection of the rights of another natural or legal person or (iv) for important reasons of public interest of the European Union or a Member State.<br></br><br></br>

                    6.	You have the right to object, at any time, on grounds relating to your personal circumstances, to processing of your Data based on the public interest or legitimate interests pursued by OPTCHAIN.<br></br><br></br>

                    OPTCHAIN will therefore no longer process your Data, unless OPTCHAIN can demonstrate compelling legitimate grounds for the processing that override your interests and rights and freedoms or for the establishment, exercise or defence of legal claims.<br></br><br></br>

                    7.	You also have the right to object, at any time, to the processing of your Data for the purpose of prospecting.<br></br><br></br>

                    You have the right to receive from OPTCHAIN your Data, in a structured, commonly used and machine-readable format, for the purpose of transferring it to another controller where:<br></br>
                    •	the processing is based on your consent or on a contract, and<br></br>
                    •	the processing is carried out using automated processes.<br></br>
                    In this respect, you have the right to have your Data transmitted directly by OPTCHAIN to another data controller where technically possible.<br></br>


                </Text>
            </Stack>
            <br></br>
            <Heading style={{textAlign: ""}} as="h1" size="sm">
                ARTICLE 9. SECURITY MEASURES
            </Heading>
            <br></br>
            <Stack spacing={3}>
                <Text fontSize='md'>
                    In view of the nature of the Data and the risks that the processing presents, OPTCHAIN takes the technical, physical and organizational measures necessary to preserve the security and confidentiality of the said Data and to prevent them from being distorted, damaged or accessed by unauthorized third parties.<br></br><br></br>

                    OPTCHAIN chooses subcontractors and service providers that present guarantees in terms of quality, security, reliability and resources to ensure the implementation of technical and organizational measures, including those relating to the security of processing.
                </Text>
            </Stack>
        </>
    );
}

export default PrivacyPolicy;