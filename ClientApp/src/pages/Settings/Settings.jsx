import React, {useState} from 'react';
import {
    Box,
    Heading,
    Image,
    Button,
    Flex,
    Text,
    Input,
    Select, Menu
} from "@chakra-ui/react";
import {MdOutlineFileUpload, MdVpnKey} from "react-icons/md";
import {useRef} from 'react';
import store from "../../utils/store";

const Settings = () => {
    const {user} = store.getState();
    const [userProfile, setUserProfile] = useState(user);

    const inputRef = useRef(null);

    const handleClick = () => {
        inputRef.current.click();
    };
    const handleFileChange = event => {
        const fileObj = event.target.files && event.target.files[0];
        if (!fileObj) {
            return;
        }

        setUserProfile({...userProfile, photoUrl: URL.createObjectURL(event.target.files[0])});
    };
    return (
        <>
            <Heading>Settings</Heading>
            <br></br>
            <Flex gap={5}>
                <Box mr={10}>
                    <Image src={userProfile.photoUrl} width={150} height={150}
                           fallbackSrc='https://via.placeholder.com/150'/>
                    <Box mt={3}>
                        <input
                            style={{display: 'none'}}
                            ref={inputRef}
                            type="file"
                            onChange={handleFileChange}
                        />
                        <Button onClick={handleClick} leftIcon={<MdOutlineFileUpload/>} colorScheme='blue'
                                variant='solid'>
                            Change picture
                        </Button>
                    </Box>
                    <Box>
                        <Text fontSize="10px">
                            Max file is 10Mb.
                        </Text>
                    </Box>
                </Box>
                <Box>
                    <Flex gap={5} mb={3} justify={"space-between"}>
                        <Box>
                            <Text>Password </Text>
                        </Box>
                        <Input onChange={(event) => setUserProfile({...userProfile, displayName: event.target.value})}
                               backgroundColor={"gray.100"} width={520} value={userProfile.displayName}/>

                    </Flex>
                    <Flex gap={5} mb={3} justify={"space-between"}>
                        <Box>
                            <Text>Display Name </Text>
                            <Text fontSize="10px">
                                This name will be part of your public profile.
                            </Text>
                        </Box>
                        <Input onChange={(event) => setUserProfile({...userProfile, displayName: event.target.value})}
                               backgroundColor={"gray.100"} width={520} value={userProfile.displayName}/>

                    </Flex>
                    <Flex gap={5} mb={3} justify={"space-between"}>
                        <Box>
                            <Text>Email </Text>
                        </Box>
                        <Input onChange={(event) => setUserProfile({...user, email: event.target.value})}
                               width={520} backgroundColor={"gray.100"} value={user.email}/>
                    </Flex>
                </Box>
            </Flex>
        </>
    );
}

export default Settings;