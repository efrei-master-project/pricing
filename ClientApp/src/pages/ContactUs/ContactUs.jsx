import React from 'react';
import {
    Box, Button,
    FormControl,
    FormLabel, Heading,
    Input, NumberDecrementStepper, NumberIncrementStepper,
    NumberInput,
    NumberInputField,
    NumberInputStepper, Slider, SliderFilledTrack, SliderThumb, SliderTrack,
    Stack,
    Flex,
    Text
} from "@chakra-ui/react";
import {Link} from "react-router-dom";
import {MdQueryBuilder} from "react-icons/md";

const ContactUs = () => {
    
    return (
    <>
        <Heading style={{textAlign: "center"}}>Contact Us</Heading><br></br>
        
        <Heading style={{textAlign: "center"}} as="h1" size="xs">
            Hello there. How can we help?
        </Heading>
        <br></br>
        <Heading style={{textAlign: "center"}} as="h2" size="sm">
            If you’re experiencing product issues, not to worry contact our Support Team. We’ll be in touch as soon as we can.
        </Heading>
        <br></br>
        <Box style={{textAlign: "center"}}>
            
        <Stack spacing={2} style={{textAlign: "center"}}>

            <Flex direction="row" spacing={40} style={{textAlign: "center"}}>
                <Box  margin={"auto"}>
                    <Stack direction="row" spacing={4} style={{textAlign: "center"}}>
                        <FormControl width={250}>
                            <FormLabel  textAlign={"center"}></FormLabel>
                            <Input placeholder={"First Name"} type='string'></Input>
                        </FormControl>
                        <FormControl width={250}>
                            <FormLabel  textAlign={"center"}></FormLabel>
                            <Input placeholder={"Last Name"} type='string' />
                        </FormControl>
                    </Stack>
                </Box>
            </Flex>
            
            <Flex direction="row" spacing={40} style={{textAlign: "center"}}>
                <Box  margin={"auto"}>
                        <FormControl width={520}>
                            <FormLabel textAlign={"center"}></FormLabel>
                            <Input placeholder={"Mail adress"}  type='string' />
                        </FormControl>
                </Box>
            </Flex>
            <Flex direction="row" spacing={40} style={{textAlign: "center"}}>
                <Box  margin={"auto"}>
                    <FormControl width={520}>
                        <FormLabel textAlign={"center"}></FormLabel>
                        <Input placeholder={"Company"}  type='string' />
                    </FormControl>
                </Box>
            </Flex>
            <Flex direction="row" spacing={40} style={{textAlign: "center"}}>
                <Box  margin={"auto"}>
                    <FormControl width={520}>
                        <FormLabel textAlign={"center"}></FormLabel>
                        <Input placeholder={"Enquiry message"}  type='string' />
                    </FormControl>
                </Box>
            </Flex>
            
            
            <br></br>
            <Box style={{textAlign: "center"}}>
                <Button width={520} colorScheme='blue'>Submit</Button>
            </Box>
           <Box>
               
               <Link to={"/privacy-policy"} style={{width: "100%", textDecoration:"none"}}>
                   <Text  fontSize="10px">
                       By providing your information you agree to our privacy policy and our terms of use.
                   </Text>
               </Link>
               
           </Box>
        </Stack>
    </Box>
    </>
    );
}

export default ContactUs;