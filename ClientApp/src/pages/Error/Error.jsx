import {Flex, Box, Heading, Button} from "@chakra-ui/react";
import {Link} from "react-router-dom";

const Error = () => {
    return (
        <Flex>
            <Box margin={"auto"}>
                <Heading margin={"5rem"}>Page not found</Heading>
                <Flex>
                    <Link to="/dashboard" style={{margin:"auto"}}><Button>Dashboard</Button></Link>
                </Flex>
            </Box>
        </Flex>
    )
}


export default Error;