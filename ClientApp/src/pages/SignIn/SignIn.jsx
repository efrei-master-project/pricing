import {Button, FormControl, FormLabel, Input, Flex, Box, Text} from "@chakra-ui/react";
import {useState} from "react";
import {getAuth, signInWithEmailAndPassword} from "firebase/auth";
import Alert from '../../components/Alert';
import {useNavigate} from "react-router";
import {Link} from "react-router-dom";
import store from "../../utils/store";

const SignIn = () => {
    const [isSubmitting, setIsSubmitting] = useState(false)
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [error, setError] = useState('')
    const navigate = useNavigate();

    return (
        <Flex>
            <Box margin={"auto"} border={"1px solid"} borderColor={" gray.300"} borderRadius={"10px"} padding={10}>
            <form onSubmit={async event => {
                event.preventDefault();
                setIsSubmitting(true);
                try {
                    const response = await fetch("user/sign-in", {
                        method: "POST",
                        body: JSON.stringify({
                            password: password,
                            email: email
                        }),
                        headers: { 'Content-Type': 'application/json' }
                    });
                    
                    const signInResponse = await response.json();
                    
                    if(!response.ok){
                        throw new Error(signInResponse.Message);
                    }
                    
                    const token = signInResponse.token;

                    const userResponse = await fetch("user");
                    const user = await userResponse.json();
                    console.log(user);
                    if (user.isEmailVerified) {
                        store.dispatch({type: 'sign-in', user: {email:user.email, photoUrl: user.photoUrl, uid : user.localId , name :  user.displayName, token:token}});
                        navigate('/dashboard');
                        window.location.reload();
                    } else {
                        setError("Please verify your account !")
                    }
                    setIsSubmitting(false);
                }catch (reason){
                    setIsSubmitting(false);
                    setError(reason.message)
                }
            }}>
                {error && <Alert message={error}/>}
                <FormControl>
                    <FormLabel htmlFor='email'>Email address</FormLabel>
                    <Input id='email' type='email' required onChange={event => setEmail(event.currentTarget.value)}/>
                </FormControl>
                <FormControl>
                    <FormLabel htmlFor='password'>Password</FormLabel>
                    <Input id='password' type='password' required onChange={event => setPassword(event.currentTarget.value)}/>
                </FormControl>
                <Text mt={3}>You doesn't have an account ? Please <Link to="/sign-up" style={{"color":'var(--chakra-colors-teal-500)'}}>sign up</Link></Text>
                <Button
                    mt={4}
                    colorScheme='teal'
                    isLoading={isSubmitting}
                    type='submit'
                >
                    Submit
                </Button>
            </form>
            </Box>
        </Flex>
    )
}


export default SignIn;