import React, { useState }  from 'react';
import {
    Box,
    Button,
    Grid,
    Heading,
    Flex,
    FormControl, FormLabel, Input, Menu,
    MenuButton,
    MenuList,
    MenuItem,
    Select,
} from "@chakra-ui/react";

const Calculator = () => {

    const [num, setNum] = useState("");
    const [result, setResult] = useState(0);

    function inputNum(e) {
        var input = e.target.value;
        setNum(num + ""+input);
    }
    function clear() {
        setNum("");
    }

    function calculate() {
        let calcul = eval(num);

        if(calcul === Infinity)calcul = "Error"
        setResult(calcul);
    }
    return (
        <>
            <Flex gap={215} marginLeft={40}>
                <Heading style={{textAlign: "center"}}>Converter</Heading><br></br>
                <Heading style={{textAlign: "center"}}>Calculator</Heading><br></br>
            </Flex>
            <br></br>
            <Flex gap={100}>
                <Box>
                    <Flex gap={255}>
                        <Heading style={{textAlign: "left"}} as="h1" size="xs">
                            From
                        </Heading>
                        <Heading style={{textAlign: "left"}} as="h1" size="xs">
                            To
                        </Heading>
                    </Flex>
                    <Flex gap={85}>
                        <Menu>
                            <Select placeholder='Devise 1'>
                                <option value='option1'>US Dollar(USD)</option>
                                <option value='option2'>Euro (EUR)</option>
                                <option value='option3'>Livre sterling (GBP)</option>
                                <option value='option3'>Franc suisse (CHF)</option>
                                <option value='option3'>Dinar jordanien (JOD)</option>
                            </Select>
                        </Menu>

                        <Menu>
                            <Select placeholder='Devise 2'>
                                <option value='option1'>US Dollar(USD)</option>
                                <option value='option2'>Euro (EUR)</option>
                                <option value='option3'>Livre sterling (GBP)</option>
                                <option value='option3'>Franc suisse (CHF)</option>
                                <option value='option3'>Dinar jordanien (JOD)</option>

                            </Select>
                        </Menu>
                    </Flex>
                    <br></br>
                    <Heading style={{textAlign: "left"}} as="h1" size="xs">
                        Enter amount
                    </Heading>
                    <Box>
                        <FormControl width={500}>
                            <FormLabel textAlign={"Enter amount"}></FormLabel>
                            <Input placeholder={"Enter amount"} type='number' />
                        </FormControl>
                    </Box>

                    <br></br>
                    <Button size="lg" textColor={"black"} colorScheme='blue'>Convertion</Button>
                    <br></br><br></br>
                    <Heading style={{textAlign: "left"}} as="h1" size="xs">
                        Result
                    </Heading>
                    <Box>
                        <FormControl width={500}>
                            <FormLabel textAlign={"Enter amount"}></FormLabel>
                            <Input  type='number'/>
                        </FormControl>
                    </Box>
                </Box>
                <Box>
                    
                    <Heading style={{textAlign: "left"}} as="h1" size="xs">
                        Enter amount
                    </Heading>

                    <Box>
                        <FormControl width={500}>
                            <FormLabel textAlign={"Enter amount"}></FormLabel>
                            <Input value={num}/>
                        </FormControl>
                    </Box>
                    <br></br>

                    <Grid gridTemplateColumns={'repeat(4,2fr)'} gap={3}
                          style={{marginRight: '20px', width: '500px'}}>

                        <Button size="sm" textColor={"black"} colorScheme='gray'>(</Button>
                        <Button size="sm" textColor={"black"} colorScheme='gray'>)</Button>
                        <Button size="sm" textColor={"black"} colorScheme='gray' onClick={inputNum}>%</Button>
                        <Button size="sm" textColor={"black"} colorScheme='gray' onClick={clear}>AC</Button>

                        <Button size="sm" textColor={"black"} colorScheme='gray' onClick={inputNum}
                                value={7}>7</Button>
                        <Button size="sm" textColor={"black"} colorScheme='gray' onClick={inputNum}
                                value={8}>8</Button>
                        <Button size="sm" textColor={"black"} colorScheme='gray' onClick={inputNum}
                                value={9}>9</Button>
                        <Button size="sm" textColor={"black"} colorScheme='gray' onClick={inputNum}
                                value="/">÷</Button>

                        <Button size="sm" textColor={"black"} colorScheme='gray' onClick={inputNum}
                                value={4}>4</Button>
                        <Button size="sm" textColor={"black"} colorScheme='gray' onClick={inputNum}
                                value={5}>5</Button>
                        <Button size="sm" textColor={"black"} colorScheme='gray' onClick={inputNum}
                                value={6}>6</Button>
                        <Button size="sm" textColor={"black"} colorScheme='gray' onClick={inputNum}
                                value="*">x</Button>

                        <Button size="sm" textColor={"black"} colorScheme='gray' onClick={inputNum}
                                value={1}>1</Button>
                        <Button size="sm" textColor={"black"} colorScheme='gray' onClick={inputNum}
                                value={2}>2</Button>
                        <Button size="sm" textColor={"black"} colorScheme='gray' onClick={inputNum}
                                value={3}>3</Button>
                        <Button size="sm" textColor={"black"} colorScheme='gray' onClick={inputNum}
                                value="-">-</Button>

                        <Button size="sm" textColor={"black"} colorScheme='gray' onClick={inputNum}
                                value={0}>0</Button>
                        <Button size="sm" textColor={"black"} colorScheme='gray' onClick={inputNum}
                                value={"."}>.</Button>
                        <Button size="sm" textColor={"black"} colorScheme='blue' onClick={calculate}>=</Button>
                        <Button size="sm" textColor={"black"} colorScheme='gray' onClick={inputNum}
                                value="+">+</Button>

                    </Grid>
                    <br></br>
                    <Heading style={{textAlign: "left"}} as="h1" size="xs">
                        Result
                    </Heading>
                    <h1 className="result">{result}</h1>

                </Box>
            </Flex>
        </>

    );
}

export default Calculator;

