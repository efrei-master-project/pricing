import React, {createContext} from 'react';
import {createRoot} from 'react-dom/client';
import {BrowserRouter} from 'react-router-dom';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import {ChakraProvider} from "@chakra-ui/react";
import store from './utils/store';
import {Provider} from 'react-redux'
import FirebaseProvider from "./utils/firebase";


const baseUrl = document.getElementsByTagName('base')[0].getAttribute('href');
const rootElement = document.getElementById('root');
const root = createRoot(rootElement);



root.render(
    <Provider store={store}>
        <FirebaseProvider>
            <ChakraProvider>
                <BrowserRouter basename={baseUrl}>
                    <App/>
                </BrowserRouter>
            </ChakraProvider>
        </FirebaseProvider>
    </Provider>
);

registerServiceWorker();

